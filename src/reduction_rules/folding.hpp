#pragma once

#include "reverse_rule.hpp"
#include "parameter_reverse_rule.hpp"
#include "../graph/directed_graph.hpp"
#include <cassert>

bool isCherry(const directed_graph &g, const directed_graph::vertex_t &x);
/**
 * @brief removes u \in V(G) such that u is a cherry
 * !!important the reduction doesnt add anything into the solution, to interpret the result, the reverse rule need to be applied at the end
 * the default variant that doesnt add a reverse rule is disabled
 *
 * u has 2 neighbours, connected to them with a 2way edge and they induce a P2 with 3 vertices
 * good for removing long paths where all edges are 2way
 *
 */
class folding : public parameter_reverse_rule<directed_graph>
{
public:
    class folding_reverse : public reverse_rule<directed_graph>
    {
        using vertex = directed_graph::vertex_t;
        vertex x;
        vertex dummy;
        vertex nonDummy;

    public:
        folding_reverse(vertex degree2, vertex neighbour_u, vertex neighbour_v, vertex newDummy)
            : x(degree2), dummy(newDummy), nonDummy(newDummy == neighbour_u? neighbour_v : neighbour_u) {
                #ifdef __DEBUG__
                assert(newDummy == neighbour_u || newDummy == neighbour_v);
                #endif
            }

        virtual bool reverse_apply(directed_graph::vertex_vec_t &in_solution) override;
    };

    virtual bool apply(directed_graph &g, directed_graph::vertex_vec_t &in_solution, int &parameter,
                       reverse_rules_stack<directed_graph> &reverse_rules) override;

    virtual std::string description() const override;

private:
    void augmentGraph(directed_graph &g, int &parameter,
                      reverse_rules_stack<directed_graph> &reverse_rules,
                      directed_graph::vertex_t x);
};