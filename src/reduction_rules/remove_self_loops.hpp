#pragma once
#include "rule.hpp"
#include "../graph/directed_graph.hpp"

/*!
 * Deletes all self-loops and adds them to solution
 */
class remove_self_loops : public rule<directed_graph>
{
 public:
	virtual bool apply(directed_graph & g, directed_graph::vertex_vec_t & in_solution ) override;

	virtual std::string description() const override;
};




