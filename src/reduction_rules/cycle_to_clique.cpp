#include "cycle_to_clique.hpp"
#include "../graph/utils.hpp"
#include "../solver/vertexBranchFinder.hpp"
#include <vector>
#include <utility>
using namespace std;

using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_t = directed_graph::vertex_t;

bool cycle_to_clique::cycle_to_clique_reverse::reverse_apply(directed_graph::vertex_vec_t &in_solution)
{
	int oldk = in_solution.size();
	in_solution.erase(
		remove_if(in_solution.begin(), in_solution.end(),
				  [&](const vertex_t &v)
				  { return newClique.find(v) != newClique.end(); }),
		in_solution.end());

	int paramDiff = newClique.size() - 1;
	int newk = in_solution.size();
	assert(newk == oldk - paramDiff || newk == oldk - (paramDiff + 1));
	if (newk == oldk - (int)newClique.size())
		in_solution.push_back(replacement);

	return true;
}

//--------------------------------------------------------------------------------------------------------

void cycle_to_clique::insertClique(directed_graph &g, const vertex_vec_t &cycleV, int &parameter,
								   reverse_rules_stack<directed_graph> &reverse_rules)
{
	vertex_vec_t clique;
	for (int i = 0; i < (int)cycleV.size(); i++)
		clique.push_back(g.add_new_vertex());

	for (int i = 0; i < (int)clique.size(); i++)
		for (int j = i + 1; j < (int)clique.size(); j++)
			g.add_edge(clique[i], clique[j]), g.add_edge(clique[j], clique[i]);

	for (int i = 0; i < (int)clique.size(); i++)
		g.add_edge(clique[i], cycleV[i]), g.add_edge(cycleV[i], clique[i]);

	reverse_rules.push_back(std::make_shared<cycle_to_clique::cycle_to_clique_reverse>(clique, cycleV.front()));
	int paramDiff = clique.size() - 1;
	parameter += paramDiff;
}

bool cycle_to_clique::handleSubgraph(directed_graph &g, directed_graph &g_no_2way_edge,
									 const vertex_vec_t &componentV, int &parameter,
									 reverse_rules_stack<directed_graph> &reverse_rules)
{
	if (componentV.size() <= 2) // ignore components that are isolated, size of 2 cant even exist (would be a 2way edge)
		return false;
	directed_graph component = g_no_2way_edge.induced_subgraph(componentV.begin(), componentV.end());
	const size_t allowedAdditionalE = 3; //todo: how many edges do we allow

	if (component.get_edges_count() < component.get_vertices_count()) // doesnt create a cycle
		return false;
	if (component.get_edges_count() - component.get_vertices_count() > allowedAdditionalE) // too many 1way edges
		return false;
	if (strongly_connected_components(component).size() > 1)
		return false;

	auto allCycles = find_allMaxkCycles(component, component.get_vertices_count()+1);
	for (const auto &cycle : allCycles)
		insertClique(g, cycle, parameter, reverse_rules);

	for (auto v : componentV)
	{
		auto neigh = component.successors(v);
		for (auto other : neigh){
			g.remove_edge(v, other);
			g_no_2way_edge.remove_edge(v, other);
		}
	}

	return true;
}

bool cycle_to_clique::tryWithArticulation(directed_graph &g, directed_graph &g_no_2way_edge,
										  directed_graph mySubgraph, int &parameter,
										  reverse_rules_stack<directed_graph> &reverse_rules)
{
	using CUT = vertex_vec_t;
	using CUT_COMPONENTS = vertex_vec_t;
	vector<pair<CUT, CUT_COMPONENTS>> instances;
	getPotatoInstances(mySubgraph, true, 1, mySubgraph.get_vertices_count(), instances);
	bool applied = false;
	for (auto [cut, componentV] : instances)
	{
		vertex_vec_t cycle = componentV;
		cycle.insert(cycle.end(), cut.begin(), cut.end());

		if (handleSubgraph(g, g_no_2way_edge, cycle, parameter, reverse_rules))
			applied = true;
	}
	return applied;
}

bool cycle_to_clique::apply(directed_graph &g, directed_graph::vertex_vec_t &in_solution, int &parameter,
							reverse_rules_stack<directed_graph> &reverse_rules)
{
	(void)in_solution; // warning bypassing

	directed_graph g_no_2way_edge = g;
	remove_2cycles_from_graph(g_no_2way_edge);
	auto sccs = strongly_connected_components(g_no_2way_edge);
	bool applied = false;
	for (const auto &vec : sccs)
	{
		if (vec.size() < 3)
			continue;

		if (handleSubgraph(g, g_no_2way_edge, vec, parameter, reverse_rules))
		{
			applied = true;
			continue;
		}
		else if (tryWithArticulation(g, g_no_2way_edge,
									 g_no_2way_edge.induced_subgraph(vec.begin(), vec.end()),
									 parameter, reverse_rules))
		{
			applied = true;
			continue;
		}
	}

	return applied;
}

std::string cycle_to_clique::description() const
{
	return "cycle to clique";
}