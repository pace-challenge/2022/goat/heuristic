#include <queue>
#include "sourcesink.hpp"

using namespace std;
using vertex_t = directed_graph::vertex_t;

bool sourcesink::apply(directed_graph& g)
{
	queue<vertex_t> q;
	for(vertex_t v: g.get_vertices())
	{
		if(g.out_degree(v) == 0 || g.in_degree(v) == 0)
		{
			q.push(v);
		}
	}
	bool success = !q.empty();
	while(!q.empty())
	{
		vertex_t v = q.front();
		q.pop();
		if(!g.contains_vertex(v))
			continue;
		if(g.out_degree(v) == 0)
		{
			//sink
			for(vertex_t p : g.predecessors(v))
			{
				if(g.out_degree(p) == 1)
				{
					q.push(p);
				}
			}
			g.remove_vertex(v);
		}
		else if(g.in_degree(v) == 0)
		{
			//source
			for(vertex_t s : g.successors(v))
			{
				if(g.in_degree(s) == 1)
				{
					q.push(s);
				}
			}
			g.remove_vertex(v);
		}
		else
		{
			assert(false && "this is weird");
		}
	}
	return success;
}

std::string sourcesink::description() const
{
    return "sources and sinks";
}
