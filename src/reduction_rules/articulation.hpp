#pragma once
#include "rule.hpp"
#include "../graph/directed_graph.hpp"

/**
 * @brief if there exists an articulation on symmetrization of graph, then try to cut off a small chunk of it
 * todo: super slow, dont apply immediately 
 */
class articulation_rule : rule<directed_graph>
{
	size_t maxComponentSize = 10;
public:
    virtual bool apply(directed_graph &g, typename directed_graph::vertex_vec_t &in_solution) override;
	virtual std::string description() const override;
};