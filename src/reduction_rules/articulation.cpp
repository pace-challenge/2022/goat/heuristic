#include "articulation.hpp"
#include "../solver/bf.hpp"
#include "../solver/vertexBranchFinder.hpp"
#include "../graph/utils.hpp"
#include <vector>
#include <utility>
#include <cassert>
using namespace std;

using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_t = directed_graph::vertex_t;

template <typename Container, typename Iterator>
bool contains(const Container &container, Iterator start, Iterator end)
{
	while (start != end)
	{
		if (std::find(container.begin(), container.end(), *start) == container.end())
			return false;
		++start;
	}
	return true;
}

bool handleOneCut(const directed_graph &g, vertex_vec_t &solution,
				  vertex_t articulation, const vertex_vec_t &component,
				  bool &tookArticulation)
{
	vertex_vec_t component_with_AP = component;
	component_with_AP.push_back(articulation);

	vertex_vec_t solution_A = bf_solver().solve_dfvs(g.induced_subgraph(component_with_AP.begin(), component_with_AP.end()));
	if (solution_A.empty())
	{
		tookArticulation = false;
		return true;
	}

	if (contains(solution_A, component_with_AP.end() - 1, component_with_AP.end()))
	{
		solution.insert(solution.end(), solution_A.begin(), solution_A.end());
		tookArticulation = true;
		return true;
	}

	vertex_vec_t solution_B = bf_solver().solve_dfvs(g.induced_subgraph(component.begin(), component.end()));
	if (solution_B.size() + 1 == solution_A.size())
	{
		solution.insert(solution.end(), solution_B.begin(), solution_B.end());
		solution.push_back(articulation);
		tookArticulation = true;
		return true;
	}

	solution.insert(solution.end(), solution_A.begin(), solution_A.end());
	tookArticulation = false;
	return true;
}

bool articulation_rule::apply(directed_graph &g, directed_graph::vertex_vec_t &in_solution)
{
	using CUT = vertex_vec_t;
	using CUT_COMPONENTS = vertex_vec_t;
	vector<pair<CUT, CUT_COMPONENTS>> instances;
	getPotatoInstances(g, true, 1, maxComponentSize, instances);
	if (instances.empty())
		return false;

	bool applied = false;
	for (auto [cut, componentV] : instances)
	{
		assert((int)cut.size() == 1);
		vertex_t AP = cut.front();
		if (!g.contains_vertex(AP))
			continue;

		componentV.erase(
			remove_if(componentV.begin(), componentV.end(),
					  [&g](const vertex_t &v)
					  { return !g.contains_vertex(v); }),
			componentV.end());

		if (componentV.empty())
			continue;

		vertex_vec_t toDeleteFromCut;
		bool tookAP = false;
		if (!handleOneCut(g, in_solution, AP, componentV, tookAP))
			continue;

		applied = true;
		for (auto v : componentV)
			g.remove_vertex(v);
		if(tookAP)
			g.remove_vertex(AP);	
	}

	return applied;
}

std::string articulation_rule::description() const
{
	return "articulation reduction rule";
}
