#include "dominated_cycles.hpp"

#include "../graph/utils.hpp"
#include "../graph/generics.hpp"

using namespace std;
using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_set_t = directed_graph::vertex_set_t;

#include <queue>
#include <cassert>

vector<pair<vertex_t, vertex_t>> get_dominated_edges(const directed_graph & one_way_subraph, vertex_t u, vertex_t v){
    assert(one_way_subraph.contains_edge(u, v));
    directed_graph without_uv = one_way_subraph;
    without_uv.remove_vertex(u);
    without_uv.remove_vertex(v);
    vector<vertex_vec_t> wccs = weakly_connected_components(without_uv);
    if(wccs.size() == 1){
        return {};
    }
    vector<pair<vertex_t, vertex_t>> edges_to_remove;
    for(vertex_vec_t component : wccs ){
        component.push_back(u);
        component.push_back(v);
        directed_graph component_graph = one_way_subraph.induced_subgraph(component.begin(), component.end());
        if(is_DAG(component_graph)){
            for(vertex_t a: component_graph.get_vertices()){
                for(vertex_t b : component_graph.successors(a)){
                    if(a == u && b == v){
                        continue;
                    }
                    edges_to_remove.emplace_back(a, b);
                }
            }
        }
    }
    return edges_to_remove;
}

bool dominated_cycles_rule::apply(directed_graph& graph)
{
    bool applied = false;
    queue<directed_graph> q;
    enqueue_oneway_components(q, graph);
    while(!q.empty()){
        directed_graph one_way_subraph = q.front();
        q.pop();
        remove_2cycles_from_graph(one_way_subraph);
        vector<pair<vertex_t, vertex_t>> edges;
        for(const vertex_t u : one_way_subraph.get_vertices()){
            for(const vertex_t v : one_way_subraph.successors(u)){
                edges.emplace_back(u,v);
            }
        }
        for(auto [u, v] : edges){
            if(!one_way_subraph.contains_edge(u, v)){
                continue;
            }
            vector<pair<vertex_t, vertex_t>> dominated_edges = get_dominated_edges(one_way_subraph, u, v);
            for(auto [a, b] : dominated_edges){
                graph.remove_edge(a, b);
                one_way_subraph.remove_edge(a, b);
                applied = true;
            }
        }
    }
    return applied;
}

std::string dominated_cycles_rule::description() const
{
    return "dominated cycles rule";
}
