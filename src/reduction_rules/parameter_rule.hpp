#pragma once
#include <string>

template<typename Graph>
class parameter_rule
{
public:
    virtual ~parameter_rule() = default;
    /*!
     * Constructor
     * @param g - graph that is kernelized - will be modified by the rule
     * @param in_solution - vertices that are in the solution - rule might add new vertices
     * @param parameter - parameter - might be modified
     */
    virtual bool apply(Graph &g, typename Graph::vertex_vec_t &in_solution, int & parameter) = 0;

	virtual std::string description() const = 0;
};
