#pragma once

#include "rule.hpp"


/*!
 * This rule does not edit the solution
 */
template<typename Graph>
class nonsolution_rule :public rule<Graph> {
 public:
	virtual bool apply(Graph& g) = 0;
	virtual bool apply(Graph& g,  typename Graph::vertex_vec_t  & in_solution ) override {
        (void)in_solution;
        return apply(g);
    }
};
