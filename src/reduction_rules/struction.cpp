#include "struction.hpp"
#include "../graph/utils.hpp"
#include "algorithm"
#include <queue>
using namespace std;

using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_t = directed_graph::vertex_t;

void struction::struction_reverse::replaceInSolution(directed_graph::vertex_vec_t &in_solution,
													 const directed_graph::vertex_vec_t &addToSolution) const
{
	in_solution.erase(
		remove_if(in_solution.begin(), in_solution.end(),
				  [&](const vertex_t &v_ij)
				  { return newlyAdded.find(v_ij) != newlyAdded.end(); }),
		in_solution.end());

	in_solution.insert(in_solution.end(), addToSolution.begin(), addToSolution.end());
}

bool struction::struction_reverse::reverse_apply(directed_graph::vertex_vec_t &in_solution)
{
	// todo: better structure to find all v_{i, j_r}
	vertex_t v_i = -1;
	directed_graph::vertex_set_t v_jr;
	for (const auto &[v_xy, pair] : newlyAdded)
	{
		if (std::find(in_solution.begin(), in_solution.end(), v_xy) != in_solution.end())
			continue;

		auto [v_x, v_j] = newlyAdded[v_xy];
		v_jr.insert(v_j);

		if (v_i == -1)
			v_i = v_x;
#ifdef __DEBUG__
		assert(v_i == v_x);
#endif
	}

	vertex_vec_t addToSolution;
	if (v_jr.empty())
	{
		addToSolution = neighbourhood; // v_0 excluded!
	}
	else
	{
		addToSolution = neighbourhood;
		addToSolution.push_back(v_0); // v_0 included!
		addToSolution.erase(
			remove_if(addToSolution.begin(), addToSolution.end(),
					  [&v_i, &v_jr](const vertex_t &dontAdd)
					  { return dontAdd == v_i || std::find(v_jr.begin(), v_jr.end(), dontAdd) != v_jr.end(); }),
			addToSolution.end());
	}
	replaceInSolution(in_solution, addToSolution);

	return true;
}

//-------------------------------------------------------------------------------------------

vertex_vec_t struction::getOrdering(const directed_graph &g, vertex_t v_0)
{
	directed_graph neighbourhood_G = g.induced_subgraph(g.successors(v_0).begin(), g.successors(v_0).end());
	remove_2cycles_from_graph(neighbourhood_G);

	vertex_vec_t result;
	fast_map<vertex_t, int> out_degrees;

	// first sort by number of oneway edges going into and out of the vertex, then sort by total degree in the original graph
	// want to minimize the number of new edges
	using priority = pair<int, int>;
	priority_queue<pair<priority, vertex_t>> q;

	for (vertex_t v : neighbourhood_G.get_vertices())
	{
		out_degrees[v] = neighbourhood_G.out_degree(v);
		if (out_degrees[v] == 0)
		{
			priority p = {sum_degrees({v}, neighbourhood_G), sum_degrees({v}, g)};
			q.push({p, v});
		}
	}

	while (!q.empty())
	{
		auto [p, v] = q.top();
		q.pop();
		result.push_back(v);
		for (vertex_t s : neighbourhood_G.predecessors(v))
		{
			if (--out_degrees[s] == 0)
			{
				priority p = {sum_degrees({s}, neighbourhood_G), sum_degrees({s}, g)};
				q.push({p, s});
			}
		}
	}

	return result;
}

//-------------------------------------------------------------------------------------------

void struction::addVertices(directed_graph &g, const vertex_vec_t &neighbourhood, newlyAddedMapper &map, vertex_vec_t &newlyAdded)
{
	for (int i = 0; i < (int)neighbourhood.size(); i++)
	{
		for (int j = i + 1; j < (int)neighbourhood.size(); j++)
		{
			vertex_t v_i = neighbourhood[i], v_j = neighbourhood[j];
			if (g.contains_edge(v_i, v_j) && g.contains_edge(v_j, v_i))
				continue;

			vertex_t v_ij = g.add_new_vertex();
			newlyAdded.push_back(v_ij);
			map[v_ij] = {v_i, v_j};
		}
	}
}

void struction::addEdge(directed_graph &g, const newlyAddedMapper &map, const vertex_vec_t &newlyAdded)
{
	for (int ij = 0; ij < (int)newlyAdded.size(); ij++)
	{
		vertex_t v_ij = newlyAdded[ij];
		auto [i, j] = map.find(v_ij)->second;

		for (int xy = ij + 1; xy < (int)newlyAdded.size(); xy++)
		{
			vertex_t v_xy = newlyAdded[xy];
			auto [_i, _j] = map.find(v_xy)->second;

			if (i != _i)
			{
				g.add_edge(v_ij, v_xy);
				g.add_edge(v_xy, v_ij);
			}
			else if (i == _i && j != _j)
			{
				if (g.contains_edge(j, _j))
					g.add_edge(v_ij, v_xy);

				if (g.contains_edge(_j, j))
					g.add_edge(v_xy, v_ij);
			}
		}
	}
}

void struction::replaceWithNewVertices(directed_graph &g, const vertex_t &v_0, const vertex_vec_t &neighbourhood,
									   const newlyAddedMapper &map, const vertex_vec_t &newlyAdded)
{
	directed_graph::vertex_set_t forbidden(neighbourhood.begin(), neighbourhood.end());
	forbidden.insert(v_0);

	// connect the new vertices back to the rest of the graph
	for (auto v_ij : newlyAdded)
	{
		auto [v_i, v_j] = map.find(v_ij)->second;

		for (auto v : {v_i, v_j})
		{
			for (auto u : g.successors(v))
				if (forbidden.find(u) == forbidden.end())
					g.add_edge(v_ij, u);

			for (auto u : g.predecessors(v))
				if (forbidden.find(u) == forbidden.end())
					g.add_edge(u, v_ij);
		}
	}

	g.remove_vertex(v_0);
	for (auto v : neighbourhood)
		g.remove_vertex(v);
}

void struction::augmentGraph(directed_graph &g, const vertex_t &v_0,
							 int &parameter, reverse_rules_stack<directed_graph> &reverse_rules,
							 int &danger_param)
{
	auto neighbourhood = getOrdering(g, v_0); // important, the order of neighbourhood needs to be a certain order

	struction::newlyAddedMapper map; // helps convert newly added vertex to the 2 old ones
	vertex_vec_t newlyAdded;		 // keeps track of which vertices were newly added
	int oldV = g.get_vertices_count();

	addVertices(g, neighbourhood, map, newlyAdded);
	addEdge(g, map, newlyAdded);
	replaceWithNewVertices(g, v_0, neighbourhood, map, newlyAdded);

	int p = neighbourhood.size();
	int q = newlyAdded.size();
#ifdef __DEBUG__
	assert((int)g.get_vertices_count() == oldV - 1 - p + q);
#endif

	int paramDiff = -p + q;
	parameter = parameter + paramDiff;
	danger_param -= paramDiff; // increased k, take away budget to prevent cycling

	reverse_rules.push_back(std::make_shared<struction::struction_reverse>(v_0, neighbourhood, map));
}

//----------------------------------------------------------------------------

int countMissingEdges(const directed_graph &g)
{
	int ret = 0;
	vertex_vec_t vertices = g.get_vertices_vec();
	for (int i = 0; i < (int)vertices.size(); i++)
	{
		vertex_t v_i = vertices[i];
		for (int j = i + 1; j < (int)vertices.size(); j++)
		{
			vertex_t v_j = vertices[j];
			if (!g.contains_edge(v_i, v_j) || !g.contains_edge(v_j, v_i))
				ret++;
		}
	}
	return ret;
}

bool struction::canApplyOn(const directed_graph &g, const vertex_t &v_0, const int &danger_param) const
{
	if (!incident_with_only_twoWay_edge(g, v_0))
		return false;

	directed_graph neighbourHood_G = g.induced_subgraph(g.successors(v_0).begin(), g.successors(v_0).end());
	int p = neighbourHood_G.get_vertices_count();
	int q = countMissingEdges(neighbourHood_G);
	int paramDiff = -p + q;

	remove_2cycles_from_graph(neighbourHood_G);
	if (!is_DAG(neighbourHood_G))
		return false;

	// only allow paramDiff \in (-oo; danger_param]
	// either negative <=> will reduce k
	// or positive <=> increase k by danger_param at most
	// also, allow to increase k by 5 at most, dont want to use all of budget to fix 1 vertex
	if (!(paramDiff <= min(5, danger_param)))
		return false;

	directed_graph copy = g;
	// remove all one way edges from neighbourhood
	for (auto s : neighbourHood_G.get_vertices())
		for (auto t : neighbourHood_G.successors(s))
			copy.remove_edge(s, t);
	// finding reachable_simple_path on copy forces to use only oneway edges going out of neighbourhood

	for (auto v_i : g.successors(v_0))
	{
		vertex_vec_t reachable = reachable_simple_path(copy, v_i);
		directed_graph::vertex_set_t reachable_set(reachable.begin(), reachable.end()); // need fast searching

		for (auto v_j : g.successors(v_0))
		{
			if (v_i == v_j)
				continue;

			if (g.contains_edge(v_j, v_i)) // either is 2way edge and everything is alright, or check v_j->v_i later
				continue;

			if (reachable_set.find(v_j) != reachable_set.end())
				return false;
		}
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool struction::dangerous_apply(directed_graph &g, vertex_vec_t &in_solution, int &parameter,
								reverse_rules_stack<directed_graph> &reverse_rules,
								int &danger_param)
{
	(void)in_solution; // bypassing compilation warning

	auto vertices = g.get_vertices_vec();
	std::sort(vertices.begin(), vertices.end(),
			  [&g](const vertex_t &a, const vertex_t &b)
			  { return g.out_degree(a) < g.out_degree(b); });

	bool applied = false;
	for (const auto &v : vertices)
	{
		if (!g.contains_vertex(v) || !canApplyOn(g, v, danger_param))
			continue;

		// creates G'
		augmentGraph(g, v, parameter, reverse_rules, danger_param);
		applied = true;
	}
	return applied;
}

bool struction::apply(directed_graph &g, directed_graph::vertex_vec_t &in_solution, int &parameter,
					  reverse_rules_stack<directed_graph> &reverse_rules)
{
	(void)in_solution; // bypassing compilation warning

	auto vertices = g.get_vertices_vec();
	std::sort(vertices.begin(), vertices.end(),
			  [&g](const vertex_t &a, const vertex_t &b)
			  { return g.out_degree(a) < g.out_degree(b); });

	bool applied = false;
	for (const auto &v : vertices)
	{
		int new_danger_param = 0;
		if (!g.contains_vertex(v) || !canApplyOn(g, v, new_danger_param))
			continue;

		// creates G'
		augmentGraph(g, v, parameter, reverse_rules, new_danger_param);
		// will throw away danger param immediately, we always want to reduce k during safe application
		applied = true;
	}
	return applied;
}

std::string struction::description() const
{
	return "struction";
}
