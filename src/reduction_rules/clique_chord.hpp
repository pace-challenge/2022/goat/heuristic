#pragma once
#include "nonsolution_rule.hpp"
#include "../graph/directed_graph.hpp"

/*!
 * Removes one way edges that does not contribute to any directed cycle that is not diesected by k way chord
 */
class clique_chord_rule : public nonsolution_rule<directed_graph>
{
 public:
    clique_chord_rule(size_t clique_size);
	virtual bool apply(directed_graph& g) override;

	virtual std::string description() const override;
private:
    size_t clique_size;
};




