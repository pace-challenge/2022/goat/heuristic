#include <queue>
#include "edgecontraction.hpp"

using namespace std;
using vertex_t = directed_graph::vertex_t;

bool edge_contraction_rule::apply(directed_graph& g)
{
	queue<vertex_t> q;
	for(vertex_t v: g.get_vertices())
	{
		if(g.out_degree(v) == 1 || g.in_degree(v) == 1)
		{
			q.push(v);
		}
	}
	bool success = false;
	while(!q.empty())
	{
		vertex_t v = q.front();
		q.pop();
		if(!g.contains_vertex(v))
			continue;
		while(g.out_degree(v) == 1)
		{
			vertex_t succ = *g.successors(v).begin();
			if(v == succ)
				break;
			g.contract_edge_h(v,succ);
			success = true;
			v = succ;
		}
		while(g.in_degree(v) == 1)
		{
			vertex_t pred = *g.predecessors(v).begin();
			if(v == pred)
				break;
			g.contract_edge_t(pred,v);
			success = true;
			v = pred;
		}
	}
	return success;
}

std::string edge_contraction_rule::description() const
{
	return "edge contraction";
}
