#include "two_way_clique.hpp"
#include "../graph/utils.hpp"
#include <climits>
#include <algorithm>

#ifdef __DEBUG__
#include <cassert>
#endif

bool is_clique(const directed_graph &g,
               const directed_graph::vertex_t &v, const directed_graph::adj_list_t &neighbours)
{
    if (!is_clique(g, neighbours))
        return false;

    // checks for (u, v) edge only, (v, u) is already created neighbours
    for (const auto &u : neighbours)
    {
        if (!g.contains_edge(u, v))
            return false;
    }
    return true;
}

bool two_way_clique::apply(directed_graph &g, directed_graph::vertex_vec_t &in_solution)
{
    const size_t maxDegree = INT_MAX;
    
    directed_graph::vertex_vec_t perspectiveVertices;
    for (const auto &v : g.get_vertices())
    {
        if (g.in_degree(v) > maxDegree)
            continue;

        if(!incident_with_only_twoWay_edge(g, v))
            continue;

        perspectiveVertices.push_back(v);
    }

    //sorts from smallest to vertices with most edges
    using vertex = directed_graph::vertex_t;
    std::sort(perspectiveVertices.begin(), perspectiveVertices.end(),
        [&g](const vertex & a, const vertex & b){ return g.in_degree(a) < g.in_degree(b); }
    );

    bool applied = false;
    for (const auto &v : perspectiveVertices)
    {
        if(!g.contains_vertex(v))
            continue;

        if (g.in_degree(v) != g.out_degree(v))
            continue;

        if (g.in_degree(v) > maxDegree)
            continue;

        auto & neighbours = g.successors(v);
        auto cliqueCopy = neighbours;
        cliqueCopy.insert(v);
        if (!is_clique(g, cliqueCopy))
            continue;

#ifdef __DEBUG__
        assert(!g.contains_edge(v, v) && "instance contains selfloop");
#endif

        in_solution.insert(in_solution.end(), neighbours.begin(), neighbours.end());

        for (auto removeV : cliqueCopy)
            g.remove_vertex(removeV);

        applied = true;
    }

    if(applied)
        apply(g, in_solution);

    return applied;
}

std::string two_way_clique::description() const
{
    return "two way clique";
}
