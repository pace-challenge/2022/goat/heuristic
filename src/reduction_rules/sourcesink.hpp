#pragma once
#include "nonsolution_rule.hpp"
#include "../graph/directed_graph.hpp"

/*!
 * Deletes all sources and sinks in one pass, one pass is in O(n+m)
 */
class sourcesink : public nonsolution_rule<directed_graph>
{
 public:
	virtual bool apply(directed_graph& g) override;

	virtual std::string description() const override;
};




