#include "neighborhood_is_clique.hpp"
#include "../graph/utils.hpp"
using vertex_set_t = directed_graph::vertex_set_t;

vertex_set_t get_oneway_neighborhood(const directed_graph::vertex_t & v,const directed_graph & g) {
    vertex_set_t res;
    for(const auto &u: g.successors(v))
    {
        if (!g.contains_edge(u,v)){
            res.insert(u);
        }
    }
    for(const auto &u: g.predecessors(v))
    {
        if (!g.contains_edge(v,u)){
            res.insert(u);
        }
    }
    return res;
}

bool neighborhood_is_clique::apply(directed_graph &g) {
    bool applied = false;
    for (const auto &v : g.get_vertices())
    {
        vertex_set_t neighbours = get_oneway_neighborhood(v,g);

        if (is_clique(g, neighbours)) {
            for (const auto &u: neighbours) {
                if(g.contains_edge(u,v)) {
                    g.remove_edge(u,v);
                    applied = true;
                }
                else {
                    g.remove_edge(v,u);
                    applied = true;
                }
            }
        }
    }
    return applied;
}

std::string neighborhood_is_clique::description() const {
    return "Oneway neighborhood is a two-way clique";
}