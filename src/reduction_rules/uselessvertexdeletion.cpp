#include "uselessvertexdeletion.hpp"
#include <queue>

using namespace std;
bool useless_vertex_deletion::apply(directed_graph& g)
{
	size_t old_size = g.get_vertices_count();
	queue<directed_graph::vertex_t> q;
	directed_graph::vertex_set_t inq;
	for(directed_graph::vertex_t v: g.get_vertices()){
		q.push(v);
		inq.insert(v);
	}

	while(!q.empty()){
		directed_graph::vertex_t v = q.front();
		q.pop();
		inq.erase(v);
		if(is_useless(g,v)){
			for(directed_graph::vertex_t s : g.successors(v)){
				if(inq.find(s) == inq.end()){
					q.push(s);
					inq.insert(s);
				}
			}
			for(directed_graph::vertex_t p : g.predecessors(v)){
				if(inq.find(p) == inq.end()){
					q.push(p);
					inq.insert(p);
				}
			}
			g.remove_vertex(v);
		}
	}
	
	return old_size != g.get_vertices_count();
}

bool useless_vertex_deletion::is_useless(const directed_graph& g, directed_graph::vertex_t v)
{
	for(directed_graph::vertex_t p: g.predecessors(v))
		for(directed_graph::vertex_t s: g.successors(v))
			if(!g.contains_edge(p, s))
				return false;
	return !g.contains_edge(v, v);
}

std::string useless_vertex_deletion::description() const
{
	return "useless vertex deletion";
}
