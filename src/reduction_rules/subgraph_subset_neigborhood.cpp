#include <queue>
#include "subgraph_subset_neigborhood.hpp"
#include "../graph/utils.hpp"
#include "../graph/generics.hpp"

using namespace std;
using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;

bool subgraph_subset_neigborhood::apply(directed_graph& graph, directed_graph::vertex_vec_t & in_solution )
{
	bool applied = false;
    queue<directed_graph> q;
    enqueue_oneway_components(q, graph, 0);
    while(!q.empty()){
        directed_graph one_way_subraph = q.front();
        q.pop();
        remove_2cycles_from_graph(one_way_subraph);
        if(is_DAG(one_way_subraph)){
            continue;
        }
        fast_map<vertex_t, vertex_vec_t> two_way_neigborhood;
        // the vertex that will be removed must have the biggest neigborhood 
        size_t max_deg = 0;
        vertex_vec_t max_deg_vertices;
        for(vertex_t v : one_way_subraph.get_vertices()){
            assert(!graph.contains_edge(v,v)); // then the rule cant be applied
            vertex_vec_t two_way_neigborhood_v = get_two_way_neigborhood(graph, v);
            sort(two_way_neigborhood_v.begin(), two_way_neigborhood_v.end());
            two_way_neigborhood[v] = two_way_neigborhood_v;
            if(max_deg < two_way_neigborhood_v.size()){
                max_deg = two_way_neigborhood_v.size();
                max_deg_vertices = vertex_vec_t();
            }
            if(max_deg == two_way_neigborhood_v.size()){
                max_deg_vertices.push_back(v);
            }
        }
        // if we find one vertex with maximal degree that creates DAG if we remove it then it is enough - all other max deg vertices must have the same neigborhood
        vertex_t checked_vertex = -1;
        for(vertex_t v : max_deg_vertices){
            directed_graph one_way_subraph_without_v = one_way_subraph;
            one_way_subraph_without_v.remove_vertex(v);
            if(is_DAG(one_way_subraph_without_v)){
                checked_vertex = v;
                break;
            }
        }
        // no max degree vertex that makes one_way_subraph_without_v DAG
        if(checked_vertex == -1){
            continue;
        }
        vertex_vec_t the_superset = two_way_neigborhood[checked_vertex];
        bool success = true;
        for(vertex_t v : one_way_subraph.get_vertices()){
            if(!is_subset_of(two_way_neigborhood[v], the_superset)){
                success = false;
                break;
            }
        }
        if(success){
            in_solution.push_back(checked_vertex);
            graph.remove_vertex(checked_vertex);
            applied = true;
        }
    }
    return applied;
}

std::string subgraph_subset_neigborhood::description() const
{
	return "subgraph subset neigborhood";
}
