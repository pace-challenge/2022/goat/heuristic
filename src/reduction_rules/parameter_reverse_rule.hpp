#pragma once

#include "rule.hpp"
#include "reverse_rule.hpp"
#include <cassert>
template<typename Graph>
class parameter_reverse_rule : public rule<Graph>
{
	public:
	virtual bool apply(Graph &g, typename Graph::vertex_vec_t &in_solution) override
    {
        (void)g;
        (void)in_solution; // disable unused param warning
#ifdef __DEBUG__
        assert(false && "this rule might add a dummy vertex into solution, use the other version");
#endif
        return false;
    }

    virtual bool apply(Graph &g, typename Graph::vertex_vec_t &in_solution, int &parameter,
                       reverse_rules_stack<Graph> &reverse_rules) = 0;
};
