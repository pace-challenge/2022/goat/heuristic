#include "folding.hpp"
#include "../graph/utils.hpp"

#include <algorithm>
#include <queue>
using namespace std;

using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_t = directed_graph::vertex_t;

bool isCherry(const directed_graph &g, const directed_graph::vertex_t &x)
{
	if (g.out_degree(x) != 2 || g.in_degree(x) != 2)
		return false;

	const auto &neighbours = g.successors(x);
	for (const auto &neigh : neighbours)
		if (!g.contains_edge(neigh, x))
			return false;

	directed_graph::vertex_t u = *neighbours.begin();
	directed_graph::vertex_t v = *std::next(neighbours.begin());
	if (g.contains_edge(u, v) || g.contains_edge(v, u))
		return false;

	return true;
}

bool contains_simple_Path(const directed_graph &g, const directed_graph::vertex_t &u, const directed_graph::vertex_t &v)
{
	// check for one way path u -> ... -> v
	if (incident_with_out_edge(g, u) && incident_with_in_edge(g, v) && exist_simple_path(g, u, v))
		return true;

	// checks for the other path v -> ... -> u
	if (incident_with_out_edge(g, v) && incident_with_in_edge(g, u) && exist_simple_path(g, v, u))
		return true;

	return false;
}

//----------------------------------------------------------------------------------

bool folding::folding_reverse::reverse_apply(directed_graph::vertex_vec_t &in_solution)
{
	auto it = std::find(in_solution.begin(), in_solution.end(), dummy);
	in_solution.push_back(it == in_solution.end() ? x : nonDummy);
	return true;
}

void folding::augmentGraph(directed_graph &g, int &parameter,
						   reverse_rules_stack<directed_graph> &reverse_rules, directed_graph::vertex_t x)
{
	auto uv = g.successors(x);
	auto u = *uv.begin();
	auto v = *(std::next(uv.begin()));

	g.remove_vertex(x);
	g.remove_edge(u, v);
	g.remove_edge(v, u);

	vertex_t dummy = u;

	// copy edges of the neighbourhood of v to dummy
	for (const auto &other : g.successors(v))
		g.add_edge(dummy, other);

	for (const auto &other : g.predecessors(v))
		g.add_edge(other, dummy);

	g.remove_vertex(v);

	// add rule to decode what dummy actually represents
	reverse_rules.push_back(std::make_shared<folding::folding_reverse>(x, u, v, dummy));
	parameter--;
}

bool canApplyOn(const directed_graph &g, vertex_t v_0)
{
	if (g.out_degree(v_0) != 2 || g.in_degree(v_0) != 2)
		return false;

	if (!incident_with_only_twoWay_edge(g, v_0))
		return false;

	vertex_t u = *g.successors(v_0).begin(), v = *std::next(g.successors(v_0).begin());

	if (isCherry(g, v_0))
		return !contains_simple_Path(g, u, v);

	if (g.contains_edge(v, u) && g.contains_edge(u, v))
		return false;

	// WLOG it is u->v
	if (g.contains_edge(v, u))
		swap(u, v);

	directed_graph copy = g;
	copy.remove_edge(u, v);
	if (exist_simple_path(copy, u, v)) // non trivial 1way path exists, cannot safely reduce, would introduce a new cycle that wasnt there before
		return false;

	return true;
}

bool folding::apply(directed_graph &g, directed_graph::vertex_vec_t &in_solution, int &parameter,
					reverse_rules_stack<directed_graph> &reverse_rules)
{
	if (parameter < 0)
		return false;

	vertex_vec_t perspective_vertices;
	for(auto x : g.get_vertices())
		if(canApplyOn(g, x))
			perspective_vertices.push_back(x);

	// auto perspective_vertices = g.get_vertices();
	bool applied = false;
	for (const auto &x : perspective_vertices)
	{
		if (!g.contains_vertex(x) || !canApplyOn(g, x))
			continue;

#ifdef __DEBUG__
		assert(!g.contains_edge(x, x) && "found self loop");
#endif
		augmentGraph(g, parameter, reverse_rules, x);

		applied = true;
	}

	if (applied)
		apply(g, in_solution, parameter, reverse_rules);

	return applied;
}

std::string folding::description() const
{
	return "folding";
}