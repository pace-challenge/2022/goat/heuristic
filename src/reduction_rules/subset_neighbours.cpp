#include "subset_neighbours.hpp"

#include <algorithm>
#include <iostream>

using namespace std;
using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_set_v = directed_graph::vertex_set_t;

/*
 * Asks whether a is subset of b, but excludes vertex v from a
 */
bool is_subset(const vertex_set_v & a, const vertex_set_v & b, vertex_t exclude){
    for(vertex_t v : a){
        if(b.find(v) == b.end() && v != exclude){
            return false;
        }
    }
    return true;
}

bool subset_neighbours_rule::apply(directed_graph& graph, directed_graph::vertex_vec_t & in_solution )
{
    vertex_set_v to_remove;
    for(vertex_t u : graph.get_vertices()){
        for(vertex_t v : graph.successors(u)){
            if(graph.contains_edge(v,u) 
                && !graph.contains_edge(u,u) // self loop forces the vertex into solution, can't have that
                && graph.in_degree(u) <= graph.in_degree(v) 
                && graph.out_degree(u) <= graph.out_degree(v)
                && is_subset(graph.successors(u), graph.successors(v), v)
                && is_subset(graph.predecessors(u), graph.predecessors(v), v)
                ){
                // if the nighbourhood is the same we want to add only one vertice
                if(graph.in_degree(u) < graph.in_degree(v) || graph.out_degree(u) < graph.out_degree(v) || u < v){
                    to_remove.insert(v);
                }
            }
        }
    }
    for(vertex_t v : to_remove){
        graph.remove_vertex(v);
        in_solution.push_back(v);
    }
    return to_remove.size() > 0;
}

std::string subset_neighbours_rule::description() const
{
    return "subset neighbours";
}
