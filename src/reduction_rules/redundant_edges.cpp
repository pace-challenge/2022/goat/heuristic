#include "redundant_edges.hpp"

#include "../graph/utils.hpp"

using namespace std;
using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_set_t = directed_graph::vertex_set_t;

bool redundant_edges_rule::apply(directed_graph& graph)
{
    directed_graph one_way_only = graph;
    remove_2cycles_from_graph(one_way_only);
    vector<pair<vertex_t, vertex_t>> edges_to_remove = get_non_scc_edges(one_way_only);
    for(auto[u,v] : edges_to_remove){
        graph.remove_edge(u,v);
    }
    return edges_to_remove.size() > 0;
}

std::string redundant_edges_rule::description() const
{
    return "redundant edges";
}
