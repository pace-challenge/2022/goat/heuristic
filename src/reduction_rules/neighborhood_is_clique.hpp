#pragma once
#include <string>

#include "nonsolution_rule.hpp"
#include "../graph/directed_graph.hpp"

/**
 * When oneway neighbourhood of a vertex v is  a clique, it removes all edges between v and its oneway neighborhood.
 */
class neighborhood_is_clique: public nonsolution_rule<directed_graph>
{
public:
    virtual bool apply(directed_graph & g) override;

    virtual std::string description() const override;
};
