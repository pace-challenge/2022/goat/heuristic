#include<cmath>

#include "../graph/utils.hpp"
#include "../measuring/util/timer.h"

#include "combinations.hpp"

#include "edgecontraction.hpp"
#include "uselessvertexdeletion.hpp"
#include "sourcesink.hpp"
#include "remove_self_loops.hpp"
#include "two_way_clique.hpp"
#include "redundant_edges.hpp"
#include "subset_neighbours.hpp"
#include "dome_rule.hpp"
#include "chorded_component.hpp"
#include "vc_style_chord.hpp"
#include "dominated_cycles.hpp"
#include "flower.hpp"
#include "folding.hpp"
#include "struction.hpp"
#include "lpvc_rule.hpp"
#include "neighborhood_is_clique.hpp"
#include "cycle_to_clique.hpp"
#include "subgraph_subset_neigborhood.hpp"

static sourcesink ssr;
static edge_contraction_rule ecr;
static redundant_edges_rule rer;
static useless_vertex_deletion uvd;
static chorded_component_rule ccr;
static vc_style_chord vsc;
static dominated_cycles_rule dcr;
static remove_self_loops rsl;
static two_way_clique twc;
static subset_neighbours_rule snr;
static dome_rule dmr;
static flower flw;
static folding fld;
static struction struc;
static lpvc_rule lpvc;
static neighborhood_is_clique nic;
static cycle_to_clique ctc;
static subgraph_subset_neigborhood ssn;

using vertex_vec_t = directed_graph::vertex_vec_t;

int reduce_all(directed_graph & graph, vertex_vec_t & removed){
    int stack_pointer = removed.size();
    bool flag = true;
    while(flag){
        flag = false;
        flag = flag || ssr.apply(graph);
        flag = flag || ecr.apply(graph);
        flag = flag || rer.apply(graph);
        flag = flag || uvd.apply(graph);
        flag = flag || dmr.apply(graph);
        flag = flag || rsl.apply(graph, removed);
        flag = flag || twc.apply(graph, removed);
        flag = flag || nic.apply(graph);
        flag = flag || snr.apply(graph, removed);
        flag = flag || lpvc.apply(graph, removed);

		if(is_DAG(graph)) break;
    }
    return (int)removed.size() - stack_pointer;
}

int reduce_all(directed_graph & graph, directed_graph::vertex_vec_t & removed, reverse_rules_stack<directed_graph> & reverseRules)
{
    int oldk = graph.get_vertices_count(); //trivial upperbound
    int k = oldk;
    bool flag = true;
    while(flag){
        flag = false;
        k -= reduce_all(graph, removed);

        flag = flag || fld.apply(graph, removed, k, reverseRules);
		// flag = flag || ctc.apply(graph, removed, k, reverseRules);
		flag = flag || struc.apply(graph, removed, k, reverseRules);

		if(is_DAG(graph)) break;
    }
    return oldk - k;
}


int reduce_expensive(directed_graph & graph, directed_graph::vertex_vec_t & removed, reverse_rules_stack<directed_graph> & reverseRules){
	int oldk = graph.get_vertices_count(); //trivial upperbound
	int k = oldk;
    bool flag = true;
    while(flag){
        flag = false;
		k -= reduce_all(graph, removed, reverseRules);
        int stack_pointer = removed.size();
        flag = flag || ssn.apply(graph, removed);
        k -= (int)removed.size() - stack_pointer;

        //flag = flag || ccr.apply(graph); //vsc does a lot more and is only 2 times slower
        flag = flag || vsc.apply(graph);
        flag = flag || dcr.apply(graph);

		if(is_DAG(graph)) break;
    }
    return oldk - k;
}


int reduce_all(directed_graph & graph, vertex_vec_t & removed, int k, reverse_rules_stack<directed_graph> & reverseRules){
	int oldk = k;
    k -= reduce_all(graph, removed);

    bool flag = true;
    while(flag){
        flag = false;
        flag = flag || fld.apply(graph, removed, k, reverseRules);
        if(k < 0)
            break;

		flag = flag || struc.apply(graph, removed, k, reverseRules);
        if(k < 0)
            break;

        flag = flag || flw.apply(graph, removed, k);
        if(k < 0)
            break;

        k -= reduce_all(graph, removed);
        if(k < 0)
            break;

		if(is_DAG(graph)) break;
    }
    return oldk - k;
}

int dangerous_reduce_all(directed_graph & graph, vertex_vec_t & removed, reverse_rules_stack<directed_graph> & reverseRules, int dangerBudget)
{
	int oldk = graph.get_vertices_count(); //trivial upperbound
	int k = oldk;

	bool flag = true;
	bool kIncreased = false;
    while(flag){
        flag = false;
		int startk = k;
		// flag = flag || ctc.apply(graph, removed, k, reverseRules);
		flag = flag || struc.dangerous_apply(graph, removed, k, reverseRules, dangerBudget);
		k -= reduce_all(graph, removed, reverseRules);
		int endk = k;

		if(is_DAG(graph)) break;

		//param increased, proceed with caution
		if(endk >= startk){
			if(kIncreased) //k increased twice in a row, abort
				break;
			else
				kIncreased = true; //in preparation for next time
		}
		else{
			kIncreased = false; //reset counter
		}
    }
    return oldk - k;
}

const int RED_ECR_IDX = 0;
const int RED_SSR_IDX = 1;
const int RED_RSL_IDX = 2;
const int RED_RER_IDX = 3;
const int RED_UVD_IDX = 4;
const int RED_TWC_IDX = 5;
const int RED_NIC_IDX = 6;
const int RED_SNR_IDX = 7;
const int RED_LPVC_IDX = 8;

std::vector<int> reduction_idxs = {
    RED_ECR_IDX,
    RED_SSR_IDX,
    RED_RSL_IDX,
    RED_RER_IDX,
    RED_UVD_IDX,
    RED_TWC_IDX,
    RED_NIC_IDX,
    RED_SNR_IDX,
    // RED_LPVC_IDX,
};

bool reduce_heuristic_apply_reduction(directed_graph & graph, vertex_vec_t & removed, reverse_rules_stack<directed_graph> & reverseRules, int reduction_idx) {
    if(reduction_idx == RED_ECR_IDX) {
        // TREE_TIMER(tt_ecr, "tt_ecr", "");
        int gn = graph.get_vertices_count();
        bool r = ecr.apply(graph);
        int r_prog = gn - graph.get_vertices_count();
        // std::cerr<<"RED tt_ecr="<<r_prog<<" "<<r<<std::endl;
        return r;
    }
    if(reduction_idx == RED_SSR_IDX) {
        // TREE_TIMER(tt_ssr, "tt_ssr", "");
        int gn = graph.get_vertices_count();
        bool r = ssr.apply(graph);
        int r_prog = gn - graph.get_vertices_count();
        // std::cerr<<"RED tt_ssr="<<r_prog<<" "<<r<<std::endl;
        return r;
    }
    if(reduction_idx == RED_RSL_IDX) {
        // TREE_TIMER(tt_rsl, "tt_rsl", "");
        int gn = graph.get_vertices_count();
        bool r = rsl.apply(graph, removed);
        int r_prog = gn - graph.get_vertices_count();
        // std::cerr<<"RED tt_rsl="<<r_prog<<" "<<r<<std::endl;
        return r;
    }
    if(reduction_idx == RED_RER_IDX) {
        // TREE_TIMER(tt_rer, "tt_rer", "");
        int gn = graph.get_vertices_count();
        bool r = rer.apply(graph);
        int r_prog = gn - graph.get_vertices_count();
        // std::cerr<<"RED tt_rer="<<r_prog<<" "<<r<<std::endl;
        return r;
    }
    if(reduction_idx == RED_UVD_IDX) {
        // TREE_TIMER(tt_uvd, "tt_uvd", "");
        int gn = graph.get_vertices_count();
        bool r = uvd.apply(graph);
        int r_prog = gn - graph.get_vertices_count();
        // std::cerr<<"RED tt_uvd="<<r_prog<<" "<<r<<std::endl;
        return r;
    }
    if(reduction_idx == RED_TWC_IDX) {
        // TREE_TIMER(tt_twc, "tt_twc", "");
        int gn = graph.get_vertices_count();
        bool r = twc.apply(graph, removed);
        int r_prog = gn - graph.get_vertices_count();
        // std::cerr<<"RED tt_twc="<<r_prog<<" "<<r<<std::endl;
        return r;
    }
    if(reduction_idx == RED_NIC_IDX) {
        // TREE_TIMER(tt_nic, "tt_nic", "");
        int gn = graph.get_vertices_count();
        bool r = nic.apply(graph);
        int r_prog = gn - graph.get_vertices_count();
        // std::cerr<<"RED tt_nic="<<r_prog<<" "<<r<<std::endl;
        return r;
    }
    if(reduction_idx == RED_SNR_IDX) {
        // TREE_TIMER(tt_snr, "tt_snr", "");
        int gn = graph.get_vertices_count();
        bool r = snr.apply(graph, removed);
        int r_prog = gn - graph.get_vertices_count();
        // std::cerr<<"RED tt_snr="<<r_prog<<" "<<r<<std::endl;
        return r;
    }

    // too expensive
    // if(graph.get_vertices_count() < 512) {
    //     if(vsc.apply(graph)) continue;
    //     if(dcr.apply(graph)) continue;
    // }

    // if(fld.apply(graph, removed, k, reverseRules)) continue;
    // if(struc.apply(graph, removed, k, reverseRules)) continue;
    // if(flw.apply(graph, removed, k)) continue;

    assert(0);
    return 0;
}

int reduce_heuristic(directed_graph & graph, vertex_vec_t & removed, reverse_rules_stack<directed_graph> & reverseRules, std::map<int,int> & out_reduction_counter){
    int stack_pointer = removed.size();
    int oldk = graph.get_vertices_count(); //trivial upperbound
    int k = oldk;

    while(1){
        // std::cerr<<"reduce_heuristic iter"<<std::endl;
        bool r = false;
        std::sort(reduction_idxs.begin(), reduction_idxs.end(), [&out_reduction_counter](int r1, int r2) { return out_reduction_counter[r1] > out_reduction_counter[r2];});
        for(int reduction_idx :  reduction_idxs) {
            // std::cerr<<reduction_idx<<" => "<<out_reduction_counter[reduction_idx]<<std::endl;
            bool rr = reduce_heuristic_apply_reduction(graph, removed, reverseRules, reduction_idx);
            if(rr) {
                r = true;
                out_reduction_counter[reduction_idx]++;
                break;
            }
        }
        if(!r) {
            break;
        }
        // if(is_DAG(graph)) break;
    }
    return (int)removed.size() - stack_pointer;
}

