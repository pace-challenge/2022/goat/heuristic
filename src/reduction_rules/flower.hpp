#pragma once

#include "parameter_rule.hpp"
#include "../graph/directed_graph.hpp"

/*!
 * Applies flower rule, using petal function, deletes all vertices u with petal(u)>k, adds them into solution,
 * modifies parameter if needed
 */
class flower : public parameter_rule<directed_graph>
{
    bool doWork(directed_graph &g, directed_graph::vertex_vec_t &in_solution, int &parameter,
                directed_graph::vertex_t v, std::vector <directed_graph::vertex_vec_t> &bucket_q);

public:
    virtual bool apply(directed_graph &g, directed_graph::vertex_vec_t &in_solution, int &parameter) override;

    virtual std::string description() const override;


};