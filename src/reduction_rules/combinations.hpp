#pragma once

#include "../graph/directed_graph.hpp"
#include "reverse_rule.hpp"
#include <map>
/*
 * Reduces the graph until it is not possible to use any of the rules
 */
int reduce_all(directed_graph & graph, directed_graph::vertex_vec_t & removed);

int reduce_all(directed_graph & graph, directed_graph::vertex_vec_t & removed, reverse_rules_stack<directed_graph> & reverseRules);

int reduce_expensive(directed_graph & graph, directed_graph::vertex_vec_t & removed, reverse_rules_stack<directed_graph> & reverseRules);

/**
 * @return how much k has changed, remember to update your k
 */
int reduce_all(directed_graph & graph, directed_graph::vertex_vec_t & removed,
                int parameter, reverse_rules_stack<directed_graph> & reverseRules);

/**
 * @brief applies reduction that might increase the parameter k, but in total the reduction will not increase over dangerBudget
 *
 * @param dangerBudget max by how much param will be increased
 * @return by how much parameter increased after reduction
 */
int dangerous_reduce_all(directed_graph & graph, directed_graph::vertex_vec_t & removed,
				reverse_rules_stack<directed_graph> & reverseRules, int dangerBudget);

int reduce_heuristic(directed_graph & graph, directed_graph::vertex_vec_t & removed, reverse_rules_stack<directed_graph> & reverseRules, std::map<int,int> & out_reduction_counter);
