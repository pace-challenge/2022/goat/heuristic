#pragma once
#include <vector>
#include <memory>

template<typename Graph>
class reverse_rule
{
    public:
    virtual bool reverse_apply(typename Graph::vertex_vec_t &in_solution) = 0;
};

template<typename Graph>
using reverse_rules_stack = std::vector<std::shared_ptr<reverse_rule<Graph>>>;

template<typename Graph>
void resolveReverseRules(typename Graph::vertex_vec_t &in_solution, reverse_rules_stack<Graph> & reverseRules)
{
    while(!reverseRules.empty())
    {
        reverseRules.back()->reverse_apply(in_solution);
        reverseRules.pop_back();
    }
}

template<typename Graph>
void mergeReverseRules(reverse_rules_stack<Graph> & result, reverse_rules_stack<Graph> & newRules)
{
    result.insert(result.end(), newRules.begin(), newRules.end());
}