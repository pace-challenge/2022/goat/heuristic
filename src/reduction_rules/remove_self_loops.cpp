#include "remove_self_loops.hpp"

using namespace std;
using vertex_t = directed_graph::vertex_t;

bool remove_self_loops::apply(directed_graph & graph, directed_graph::vertex_vec_t & in_solution ) {
	size_t old_size = graph.get_vertices_count();
	auto vertices = graph.get_vertices();
	for(vertex_t v : vertices){
		if(graph.contains_edge(v,v)){
			graph.remove_vertex(v);
			in_solution.push_back(v);
		}
	}
	return old_size != graph.get_vertices_count();
}


std::string remove_self_loops::description() const
{
    return "remove self loops";
}
