#pragma once

#include "nonsolution_rule.hpp"
#include "../graph/directed_graph.hpp"

/*!
 * Rule contracts all edges with in or out degree 1, except for self-loops
 */
class edge_contraction_rule : public nonsolution_rule<directed_graph>
{
 public:
	virtual bool apply(directed_graph& g) override;

	virtual std::string description() const override;
};
