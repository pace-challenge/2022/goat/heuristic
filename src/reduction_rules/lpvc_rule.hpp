#pragma once
#include "rule.hpp"
#include "../graph/directed_graph.hpp"

class lpvc_rule : public rule<directed_graph>
{
 public:
	virtual bool apply(directed_graph &g, typename directed_graph::vertex_vec_t &in_solution) override;
	
	std::string description() const override;
};




