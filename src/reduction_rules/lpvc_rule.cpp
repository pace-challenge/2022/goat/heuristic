#include <queue>
#include "lpvc_rule.hpp"
#include "../graph/utils.hpp"
#include "../graph/flows/dinic.hpp"
#include "../fast_hash_table.hpp"

using vertex_t = directed_graph::vertex_t;
using namespace std;

std::string lpvc_rule::description() const
{
	return "lp 2k kernel";
}

bool lpvc_rule::apply(directed_graph& g, typename directed_graph::vertex_vec_t& in_solution)
{
	if(!hasOnly_twoWay_edges(g))
	{
		return false;
	}
	fast_map <vertex_t, vertex_t> forward_map;
	fast_map <vertex_t, vertex_t> back_map;
	vertex_t N = 0;
	for(vertex_t u: g.get_vertices())
	{
		forward_map[u] = N;
		back_map[N] = u;
		assert(!g.contains_edge(u, u)); //no self loops
		N++;
	}

#define S (2*N)
#define T (2*N+1)
#define LEFT(v) (2*(v))
#define RIGHT(v) (2*(v)+1)
#define ISLEFT(v) (!((v)&1))
#define ISRIGHT(v) ((v)&1)
	
	dinic dnc(2 * N + 2, S, T); //+2 for source and sink
	
	for(vertex_t u: g.get_vertices())
	{
		dnc.add_edge(S, LEFT(forward_map[u])); //connect source to left partition
		dnc.add_edge(RIGHT(forward_map[u]), T); //connect right partition to sink
		for(vertex_t v: g.successors(u))
			if(u < v) //bid edge u,v not seen before, use WLOG u < v
			{
				dnc.add_edge(LEFT(forward_map[u]), RIGHT(forward_map[v])); //{u,v'}
				dnc.add_edge(LEFT(forward_map[v]), RIGHT(forward_map[u])); //{v,u'}
			}
	}
	
	dnc.solve_flows();
	
	//Construct minimal vertex cover as in Konig's theorem
	//Let U be the set of unmatched vertices in LEFT
	//Let Z be the set of vertices reachable by a (possibly empty) alternating path from U
	//Then ( LEFT ∖ Z ) ∪ ( RIGHT ∩ Z ) is minimal vertex cover
	//Edge from LEFT to RIGHT is in matching iff it is forward edge with flow == 1
	//Edge from RIGHT to LEFT is matching iff it is residual edge with flow == -1
	
	std::vector<bool> Z(2 * N);
	
	queue <vertex_t> q;
	//initialize q with all vertices from LEFT that are not matched
	//these are the ones with flow equal to 0
	for(size_t edge_id: dnc.adj[S])
	{
		auto&[v, c, f] = dnc.edges[edge_id];
		if(c == 1 && f == 0) //forward edge && zero flow
			q.push(v);
	}
	//run BFS from U
	
	while(!q.empty())
	{
		auto u = q.front();
		q.pop();
		Z[u] = true;
		
		for(size_t edge_id: dnc.adj[u])
		{
			auto&[v, c, f] = dnc.edges[edge_id];
			if(v == S || v == T) //Do not enter source and sink
				continue;
			if(!Z[v]
			   && ((ISLEFT(u) && c == 1 && f == 0) //Either we are in left partition and expect non-matching edge (i.e. it is nonresidual with zero flow)
				   || (ISRIGHT(u) && c == 0 && f == -1))) //or we are in right partition and expect matching edge (i.e. it is residual with f == -1)
			{
				q.push(v);
			}
		}
	}
	
	//( LEFT ∖ Z ) ∪ ( RIGHT ∩ Z ) is vertex cover

	bool applied = false;
	for(vertex_t v = 0; v < N; ++v)
	{
		//!Z[LEFT(v)] == true iff the left copy of v is in VC,
		//Z[RIGHT(v)] == true iff the right copy of v is in VC
		int amount = !Z[LEFT(v)] + Z[RIGHT(v)];
		switch(amount)
		{
			case 2:
				in_solution.push_back(back_map[v]);
				g.remove_vertex(back_map[v]);
				applied = true;
				break;
			case 0:
				g.remove_vertex(back_map[v]);
				applied = true;
				break;
			case 1:
				break;
			default:
				assert("weird behaviour of matching");
				break;
		}
	}
	return applied;
}