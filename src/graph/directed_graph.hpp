#pragma once

#include "../fast_hash_table.hpp"
#include <vector>
#include <cassert>
#include <iterator>
#include <type_traits>


/*!
 * Class representing directed unweighted graph.
 */
class directed_graph
{
 public:
	
	using vertex_t = int;
	using vertex_vec_t = std::vector<vertex_t>;
	using vertex_set_t = fast_set<vertex_t>;
	using adj_list_t = vertex_set_t;
	
	/*!
	 * Constructor. Construct graph and adds vertices from 0 up to n - 1 (i.e. if n == 0, the graph is empty)
	 */
	directed_graph(size_t n = 0);
	/*!
	 * Retrieves vertices written in a vector. (O(n) construction from the internal set)
	 * Note that the order of the vertices is defined by the underlying unordered set.
	 */
	vertex_vec_t get_vertices_vec() const;
	/*!
	 * Retrieves vertices written in a set. (O(1) access to the internal set)
	 */
	const vertex_set_t& get_vertices() const;
	bool contains_edge(vertex_t u, vertex_t v) const;
	bool contains_vertex(vertex_t v) const;
	
	/*!
	 * Adds new vertex to this graph. Method finds not-used name for
	 * new vertex, inserts it and returns the new name.
	 * @return name of the new vertex added
	 */
	vertex_t add_new_vertex();
	/*!
	 * Adds vertex to this graph. If @v already exists in the graph, aborts.
	 */
	void add_vertex(vertex_t v);
	/*!
	 * Removes vertex @v from this graph. If @v does not exist in the graph, aborts.
	 */
	void remove_vertex(vertex_t v);
	/*!
	 * Adds edge @u -> @v to this graph. If @u -> @v already exists, does nothing.
	 */
	void add_edge(vertex_t u, vertex_t v);
	/*!
	 * Removes e dge @u -> @v from this graph. If @u -> @v does not exist, does nothing.
	 */
	void remove_edge(vertex_t u, vertex_t v);
	/*!
	 * Contracts edge @u -> @v into vertex @u. If @u -> @v does not exist, aborts.
	 */
	void contract_edge_t(vertex_t u, vertex_t v);
	/*!
	 * Contracts edge @u -> @v into vertex @v. If @u -> @v does not exist, aborts.
	 */
	void contract_edge_h(vertex_t u, vertex_t v);
	/*!
	 * Renames vertex @old_name to @new_name. If either old_name does not exist, or new_name exists, aborts.
	 */
	void rename_vertex(vertex_t old_name, vertex_t new_name);
	
	/*!
	 * Reverses edges of this graph. Effectively swaps successors and predecessors of each vertex. O(1)
	 */
	void reverse_edges();
	/*!
	 * Creates new graph whose edges are reversed.
	 */
	directed_graph reversed_edges() const;
	
	size_t get_vertices_count() const;
	size_t get_edges_count() const;
	
	/*!
	 * Induces a subgraph on given range of vertices. If there is a vertex in the range which is not in the graph, aborts.
	 */
	template<typename InputIt>
	std::enable_if_t<std::is_same_v<typename InputIt::value_type, vertex_t>, directed_graph> induced_subgraph(InputIt begin, InputIt end) const
	{
		directed_graph g;
		while(begin != end)
		{
			assert(contains_vertex(*begin) && "tried to induce subgraph on non-existent vertex");
			g.add_vertex(*begin);
			++begin;
		}
		for(vertex_t v: g.get_vertices())
		{
			for(vertex_t s: successors(v))
			{
				if(g.contains_vertex(s))
				{
					g.add_edge(v, s);
				}
			}
		}
		return g;
	}
	
	const adj_list_t& successors(vertex_t v) const;
	adj_list_t& successors(vertex_t v);
	const adj_list_t& predecessors(vertex_t v) const;
	adj_list_t& predecessors(vertex_t v);

	/**
	 * returns all neighbours of v, successors and predecessors
	 */
	vertex_set_t neighbours(vertex_t v) const;

	size_t out_degree(vertex_t v) const;
	size_t in_degree(vertex_t v) const;
 
	int applied_danger = 0;
 private:
	vertex_t next = 0; /*!< Next available vertex to be added*/
	size_t m = 0; /*!< Number of edges of the graph. */
	fast_map<vertex_t, adj_list_t> succs;
	fast_map<vertex_t, adj_list_t> preds;
	vertex_set_t vertices;
};
