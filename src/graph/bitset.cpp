#include "bitset.hpp"

_DynamicBitset::_DynamicBitset(int bits) : bits(bits), N((bits+63)/64), data(new uint64_t[N]) {
    for(int i=0;i<N;++i)data[i]=0;
}

_DynamicBitset::~_DynamicBitset() {
    delete[] data;
    data = nullptr;
}

_DynamicBitset::_DynamicBitset(_DynamicBitset && o) : bits(o.bits), N(o.N), data(o.data) {
    o.data = nullptr;
}

_DynamicBitset::_DynamicBitset(const _DynamicBitset & o) : bits(o.bits), N(o.N), data(new uint64_t[o.N]) {
    for(int i=0;i<N;++i)data[i]=o.data[i];
}

void _DynamicBitset::clear(bool v) {
    uint64_t vv = uint64_t(v) * 0xffffffffffffffffull;
    for(int i=0;i<N;++i)data[i]=vv;
}


void _DynamicBitset::set_bit(int i) {
    data[i>>6] |= ((uint64_t)1)<<(i&63);
}

void _DynamicBitset::unset_bit(int i) {
    data[i>>6] &= ~(((uint64_t)1)<<(i&63));
}

bool _DynamicBitset::has_bit(int i) const {
    return data[i>>6] & ((uint64_t)1)<<(i&63);
}

bool _DynamicBitset::operator < (const _DynamicBitset & o) const {
    for(int i = N - 1; i >= 0; --i) {
        if(data[i]!=o.data[i]) {
            return data[i] < o.data[i];
        }
    }
    return false;
}

bool _DynamicBitset::operator == (const _DynamicBitset & o) const {
    for(int i=0;i<N;++i) {
        if(data[i]!=o.data[i]) return 0;
    }
    return 1;
}

bool _DynamicBitset::operator != (const _DynamicBitset & o)  const {
    return !(*this == o);
}

_DynamicBitset _DynamicBitset::_and(const _DynamicBitset & o) const {
    _DynamicBitset r(*this);
    for(int i=0;i<N;++i) {
        r.data[i]=data[i] & o.data[i];
    }
    return r;
}

_DynamicBitset _DynamicBitset::_or(const _DynamicBitset & o) const {
    _DynamicBitset r(*this);
    for(int i=0;i<N;++i) {
        r.data[i]=data[i] | o.data[i];
    }
    return r;
}

std::vector<int> _DynamicBitset::gather_ones() const {
    std::vector<int> r;
    uint64_t t;
    for(int i=0;i<N;++i) {
        t=data[i];
        int c = __builtin_popcountll(t);
        while(c>0) {
            int idx = __builtin_ctzll(t);
            r.push_back((i<<6)+idx);
            t &= ~(1ULL<<idx);
            c--;
        }
    }
    return r;
}


