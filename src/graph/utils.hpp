#pragma once

#include "directed_graph.hpp"
#include <functional>
#include <vector>
#include <queue>


/*!
* Divides vertices of graph into its strongly connected components.
*/
std::vector<directed_graph::vertex_vec_t> strongly_connected_components(const directed_graph& graph);

/*!
* Divides vertices of graph into its weakly connected components.
*/
std::vector<directed_graph::vertex_vec_t> weakly_connected_components(const directed_graph& graph);

/*!
* Returns edges that are not in strongly connected component (from one component to other)
*/
std::vector<std::pair<directed_graph::vertex_t, directed_graph::vertex_t>> get_non_scc_edges(const directed_graph& graph);

/*!
* Divides vertices of graph into strongly connected components. On each component @f is called.
* @f must be callable on an instance of directed_graph, its result must be a container of items convertible
* to vertex_t
*/
template<typename Fun>
directed_graph::vertex_vec_t calculate_on_components(const directed_graph& graph, Fun f)
{
	std::vector <directed_graph::vertex_vec_t> sccs = strongly_connected_components(graph);
	if(sccs.size() == 1)
	{
		return f(graph);
	}
	directed_graph::vertex_vec_t result;
	for(const directed_graph::vertex_vec_t& vertices: sccs)
	{
		directed_graph component = graph.induced_subgraph(vertices.begin(), vertices.end());
		auto component_solution = f(component);
		result.insert(result.end(), component_solution.begin(), component_solution.end());
	}
	return result;
}

/*!
* Checks whether graph is DAG
*/
bool is_DAG(const directed_graph& graph);

/*!
* finds all cycles of length 1
*/
std::vector<directed_graph::vertex_vec_t> find_all1Cycles(const directed_graph& graph);

/*!
* finds all cycles of length 2
*/
std::vector<directed_graph::vertex_vec_t> find_all2Cycles(const directed_graph& graph);


/*!
* finds all one way edges, [0] is a pair u->v such that v->u does not exist in graph
*/
std::vector<directed_graph::vertex_vec_t> find_all1way_edge(const directed_graph &graph);

/*!
* finds all cycles of length k
*/
std::vector<directed_graph::vertex_vec_t> find_allkCycles(const directed_graph& graph, size_t k);

/*!
* finds all cycles of length at most k
*/
std::vector<directed_graph::vertex_vec_t> find_allMaxkCycles(const directed_graph& graph, size_t k);


/*!
* finds shortest cycle that includes the vertex v
* returns empty std::vector if no cycle exists
*/
directed_graph::vertex_vec_t find_shortest_cycle_including_v(const directed_graph& graph, directed_graph::vertex_t v);


/*!
* finds shortest cycle for each vertex, the cycles are sorted in ascending order (i.e. smallest cycles are first)
*/
std::vector<directed_graph::vertex_vec_t> find_shortest_cycle_foreach_v(const directed_graph& graph);

/*!
* removes cycles that contains all vertices of another cycle
*/
//TODO: do not pass by value, instead remove the cycles inplace
std::vector<directed_graph::vertex_vec_t> remove_redundant_cycles(std::vector<directed_graph::vertex_vec_t> cycles);

/**
 * @brief checks if there exists a one way path from start to end
 */
bool exist_simple_path(const directed_graph &g, const directed_graph::vertex_t &start, const directed_graph::vertex_t &end);

/**
 * @brief checks if ``a`` and ``b`` lie on a simple one way only, directed cycle
 */
bool is_on_one_way_cycle(const directed_graph & graph, directed_graph::vertex_t a, directed_graph::vertex_t b);

/**
 * @brief returns list of vertices reachable with only one way edges
 */
directed_graph::vertex_vec_t reachable_simple_path(const directed_graph &g, const directed_graph::vertex_t &start);

/*
 * Removes all 2 cycles from the graph and only one way edges remain
 */
void remove_2cycles_from_graph(directed_graph & graph);

/*
 * Removes all vertices that are in a 2 cycle from the graph, only vertices that are not in any two cycle remain
 */
void remove_2cycle_vertices_from_graph(directed_graph & graph);

/*
 * Computes sum of degrees of all vertices
 */
int sum_degrees(const directed_graph::vertex_vec_t & vertices, const directed_graph & graph);

/**
 * @brief checks if AT LEAST one of its edge is purely out edge
 */
bool incident_with_out_edge(const directed_graph &g, const directed_graph::vertex_t &v);

/**
 * @brief checks if AT LEAST one of its edge is purely in edge
 */
bool incident_with_in_edge(const directed_graph &g, const directed_graph::vertex_t &v);

/**
 * @return false if there exists a one way edge
 * @return true otherwise
 */
bool incident_with_only_twoWay_edge(const directed_graph &g, const directed_graph::vertex_t &v);

/**
 * @brief checks if AT LEAST one of its edges is two way, if the vertex has no neighbours, returns false
 */
bool incident_with_twoWay_edge(const directed_graph &g, const directed_graph::vertex_t &v);

bool hasOnly_twoWay_edges(const directed_graph &g);

directed_graph::vertex_vec_t get_two_way_neigborhood(const directed_graph & g, directed_graph::vertex_t v);

/**
 * @brief connect all predecessors of v to successors of v and vice-versa, then removes vertex v
 * 
 * simulates removal of v while not removing any cycles, should be used when its known that v will not be in solution
 */
void notInSolution(directed_graph & g, directed_graph::vertex_t v);

/**
 * @brief returns all vertices in topological order, 1st vertices are sinks, last vertices are sources
 */
directed_graph::vertex_vec_t topsort(const directed_graph& graph);

/*!
 * Returns the size of a petal from vertex v, that is the number of vertex-disjoint cycles going through v.
 */
size_t petal(const directed_graph& graph, directed_graph::vertex_t v);

/*!
 * Decides whether the size of a petal from vertex v is of size at most k.
 */
std::pair<bool,size_t> petal(const directed_graph& graph, directed_graph::vertex_t v, size_t k);

bool is_clique(const directed_graph & g, const directed_graph::vertex_set_t & in);


/*!
* finds all cliques of size k
*/
std::vector<directed_graph::vertex_vec_t> find_allkCliques(const directed_graph& graph, size_t k);



/*!
* Finds clique that can't be made larger by adding vertices
*/
directed_graph::vertex_set_t heuristic_maximal_clique(const directed_graph& graph);

/*!
* Finds clique that can't be made larger by adding vertices, probably faster version, but not that high quality
*/
directed_graph::vertex_vec_t heuristic_maximal_clique_2(const directed_graph& graph);
/**
 * @brief get all articulations in graph
 */
std::vector<directed_graph::vertex_vec_t> get_articulation_points(const directed_graph & graph);

/**
 * @brief get all vertexCuts of size 2
 */
std::vector<directed_graph::vertex_vec_t> get2Cut(const directed_graph & g);

/**
 * returns all vertex cuts that are of size exactly cutSize
 * depending on the template, will use cuts for weakly connected components or strongly connected components
 */
template<bool weaklyConnected>
std::vector<directed_graph::vertex_vec_t> getVertexCut(const directed_graph & g, int cutSize);


void enqueue_oneway_components(std::queue<directed_graph> & q, const directed_graph & graph, size_t min_component_size = 3);