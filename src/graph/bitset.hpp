#pragma once
#include <cstdint>
#include <vector>

struct _DynamicBitset {
    const int bits;
    const int N;
    uint64_t * data;

    _DynamicBitset(int bits);

    ~_DynamicBitset();
    _DynamicBitset(_DynamicBitset && o);
    _DynamicBitset& operator=(const _DynamicBitset& o) = delete;

    _DynamicBitset(const _DynamicBitset & o);

    void clear(bool v);

    void set_bit(int i);

    void unset_bit(int i);

    bool has_bit(int i) const;

    bool operator < (const _DynamicBitset & o) const;

    bool operator == (const _DynamicBitset & o) const;

    bool operator != (const _DynamicBitset & o)  const;
    _DynamicBitset _and(const _DynamicBitset & o) const;
    _DynamicBitset _or(const _DynamicBitset & o) const;

    std::vector<int> gather_ones() const;
};

