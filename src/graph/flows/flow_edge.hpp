#pragma once
#include "../directed_graph.hpp"
using flow_t = int;

struct flow_edge
{
	flow_edge(directed_graph::vertex_t v, flow_t c);
	directed_graph::vertex_t v;
	flow_t c, f = 0;
};



