#include "dinic.hpp"

using vertex_t = directed_graph::vertex_t;


dinic::dinic(size_t n, vertex_t s, vertex_t t) : adj(n), level(n), starts(n), s(s), t(t) { }

void dinic::add_edge(vertex_t u, vertex_t v, flow_t c)
{
	adj[u].push_back(edges.size());
	edges.emplace_back(v, c);
	adj[v].push_back(edges.size());
	edges.emplace_back(u, 0);
}

bool dinic::bfs()
{
	for(auto& l: level)
		l = -1;
	level[s] = 0;
	q.push(s);
	while(!q.empty())
	{
		vertex_t u = q.front();
		q.pop();
		for(size_t edge_id: adj[u])
		{
			auto&[v, c, f] = edges[edge_id];
			if(level[v] == -1 && f < c)
			{
				level[v] = level[u] + 1;
				q.push(v);
			}
		}
	}
	return level[t] >= 0;
}

flow_t dinic::dfs(directed_graph::vertex_t u, flow_t flow)
{
	if(u == t)
		return flow;
	if(level[u] >= level[t] || flow == 0)
		return 0;
	for(; starts[u] != adj[u].end(); ++starts[u])
	{
		size_t edge_id = *starts[u];
		auto&[v, c, f] = edges[edge_id];
		if(level[v] == level[u] + 1 && f < c)
		{
			flow_t forward_flow = dfs(v, std::min(flow, c - f));
			if(forward_flow > 0)
			{
				edges[edge_id].f += forward_flow;
				edges[edge_id ^ 1].f -= forward_flow;
				return forward_flow;
			}
		}
	}
	return 0;
}

void dinic::solve_flows()
{
	while(bfs())
	{
		size_t i = 0;
		for(auto& adj_list: adj)
			starts[i++] = adj_list.begin();
		while(dfs(s, std::numeric_limits<flow_t>::max()));
	}
}