#include "../utils.hpp"

using namespace std;
using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;

void enqueue_oneway_components(queue<directed_graph> & q, const directed_graph & graph, size_t skip_components_upto_this_size){
    directed_graph one_way_only = graph; 
    remove_2cycles_from_graph(one_way_only);
	vector<vertex_vec_t> sccs = strongly_connected_components(one_way_only);
    for(const vertex_vec_t & component : sccs){
        if(component.size() <= skip_components_upto_this_size){
            continue; // component does not have any chord
        }
        directed_graph subgraph = graph.induced_subgraph(component.begin(), component.end());
        q.push(subgraph);
    }
}

vector<vertex_vec_t> find_all1way_edge(const directed_graph &graph)
{
	vector <vertex_vec_t> result;
	for(vertex_t u: graph.get_vertices())
	{
		for(vertex_t v: graph.successors(u))
		{
			if(!graph.contains_edge(v, u))
				result.push_back({u, v});
		}
	}
	return result;
}