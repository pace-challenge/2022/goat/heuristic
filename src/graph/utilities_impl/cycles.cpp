#include <queue>
#include "../utils.hpp"
#include "../generics.hpp"

using namespace std;
using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;
const int undefined = -1;
vector <vertex_vec_t> find_all1Cycles(const directed_graph& graph)
{
	vector <vertex_vec_t> result;
	for(vertex_t v: graph.get_vertices())
	{
		if(graph.contains_edge(v, v))
		{
			result.push_back({v});
		}
	}
	return result;
}


vector <vertex_vec_t> find_all2Cycles(const directed_graph& graph)
{
	vector <vertex_vec_t> result;
	for(vertex_t u: graph.get_vertices())
	{
		for(vertex_t v: graph.successors(u))
		{
			if(u < v && graph.contains_edge(u, v) && graph.contains_edge(v, u))
			{
				result.push_back({u, v});
			}
		}
	}
	return result;
}


void find_allkCycles_inner(const directed_graph& graph, size_t k,
						   fast_map<vertex_t, bool> visited,
						   vertex_vec_t& current_cycle, vector <vertex_vec_t>& result)
{
	if(current_cycle.size() == k)
	{
		if(graph.contains_edge(current_cycle[k - 1], current_cycle[0]))
		{
			result.push_back(current_cycle);
		}
		return;
	}
	else if(current_cycle.size() == 0)
	{
		for(vertex_t v: graph.get_vertices())
		{
			current_cycle.push_back(v);
			visited[v] = true;
			find_allkCycles_inner(graph, k, visited, current_cycle, result);
			current_cycle.pop_back();
			visited[v] = false;
		}
	}
	else
	{
		for(vertex_t v: graph.successors(current_cycle.back()))
		{
			if(visited[v])
				continue;
			if(v <= current_cycle[0])
			{ // the first vertex must be the smallest to not find duplicates
				continue;
			}
			current_cycle.push_back(v);
			visited[v] = true;
			find_allkCycles_inner(graph, k, visited, current_cycle, result);
			current_cycle.pop_back();
			visited[v] = false;
		}
	}
}

vector <vertex_vec_t> find_allkCycles(const directed_graph& graph, size_t k)
{
	if(k == 0)
	{
		throw;
	}
	else if(k == 1)
	{
		return find_all1Cycles(graph);
	}
	else if(k == 2)
	{
		return find_all2Cycles(graph);
	}
	vertex_vec_t acc;
	vector <vertex_vec_t> result;
	fast_map<vertex_t, bool> visited;
	for(auto v : graph.get_vertices())
		visited[v] = false;
	find_allkCycles_inner(graph, k, visited, acc, result);
	return result;
}


vector <vertex_vec_t> find_allMaxkCycles(const directed_graph& graph, size_t k)
{
	vector <vertex_vec_t> result;
	for(size_t i = 1; i < k; ++i)
	{
		vector <vertex_vec_t> icycles = find_allkCycles(graph, i);
		result.insert(result.end(), icycles.begin(), icycles.end());
	}
	return result;
}


vertex_vec_t reconstruct_cycle(const fast_map<vertex_t, vertex_t>& parents, vertex_t start)
{
	vertex_vec_t cycle = {start};
	vertex_t current = parents.find(start)->second;
	while(current != start)
	{
		cycle.push_back(current);
		current = parents.find(current)->second;
	}
	return cycle;
}

vertex_vec_t find_shortest_cycle_including_v(const directed_graph& graph, vertex_t v)
{
	fast_map<vertex_t, vertex_t> parent;
	for(vertex_t u: graph.get_vertices())
		parent[u] = undefined;
	queue<vertex_t> q;
	q.push(v);
	while(!q.empty())
	{
		vertex_t current = q.front();
		q.pop();
		for(vertex_t s: graph.successors(current))
		{
			if(parent[s] == undefined)
			{
				parent[s] = current;
				q.push(s);
			}
			if(s == v)
			{
				break;
			}
		}
	}
	if(parent[v] == undefined)
	{
		return vertex_vec_t {};
	}
	return reconstruct_cycle(parent, v);
}


vector <vertex_vec_t> find_shortest_cycle_foreach_v(const directed_graph& graph)
{
	vector <pair<vertex_t, vertex_vec_t>> owned_cycles; // cycle owner then the cycle
	for(vertex_t v: graph.get_vertices())
	{
		owned_cycles.push_back({v, find_shortest_cycle_including_v(graph, v)});
	}
	sort(owned_cycles.begin(), owned_cycles.end(),
		 [](const pair<vertex_t, vertex_vec_t>& a, const pair<vertex_t, vertex_vec_t>& b) {
			 return a.second.size() < b.second.size();
		 });
	vector <vertex_vec_t> cycles;
	directed_graph::vertex_set_t visited;
	for(const auto&[owner, cycle]: owned_cycles)
	{
		if(visited.find(owner) != visited.end() || cycle.size() == 0)
		{
			continue;
		}
		for(vertex_t v: cycle)
		{
			visited.insert(v);
		}
		cycles.push_back(move(cycle));
	}
	return cycles;
}

/*!
 * Checks if range A is subset of range B (the ranges must be sorted, otherwise undefined behaviour).
 */


vector <vertex_vec_t> remove_redundant_cycles(vector <vertex_vec_t> cycles)
{
	for(size_t i = 0; i < cycles.size(); ++i)
	{
		sort(cycles[i].begin(), cycles[i].end());
	}
	sort(cycles.begin(), cycles.end(),
		 [](const vertex_vec_t& a, vertex_vec_t& b) {
			 return a.size() < b.size();
		 });
	vector <vertex_vec_t> result;
	for(vertex_vec_t& to_add: cycles)
	{
		bool add = true;
		for(vertex_vec_t& added_cycle: result)
		{
			add = add && !is_subset_of(added_cycle, to_add);
		}
		if(add)
		{
			result.push_back(to_add);
		}
	}
	return result;
}


void remove_2cycles_from_graph(directed_graph & graph){
	vector<vertex_vec_t> two_cycles = find_all2Cycles(graph);
    for(const vertex_vec_t & two_cycle : two_cycles){
        graph.remove_edge(two_cycle[0], two_cycle[1]);
        graph.remove_edge(two_cycle[1], two_cycle[0]);
    }
}


void remove_2cycle_vertices_from_graph(directed_graph & graph){
	vector<vertex_vec_t> two_cycles = find_all2Cycles(graph);
    for(const vertex_vec_t & two_cycle : two_cycles){
        for(int i = 0; i <= 1; ++i){
            if(graph.contains_vertex(two_cycle[i])){
                graph.remove_vertex(two_cycle[i]);
            }
        }
    }
}

bool exist_simple_path(const directed_graph &g, const directed_graph::vertex_t &start, const directed_graph::vertex_t &end)
{
    using vertex = directed_graph::vertex_t;
    queue<vertex> q;
    fast_map<vertex, bool> visited;
    for (const auto & v : g.get_vertices())
        visited[v] = false;

    q.push(start);
    visited[start] = true;

    while (!q.empty())
    {
        auto cur = q.front();
        q.pop();
        for (const auto & neigh : g.successors(cur))
        {
            if (g.contains_edge(neigh, cur)) // ignore 2way edge
                continue;
            if (neigh == end)
                return true;
            if (visited[neigh])
                continue;

            visited[neigh] = true;
            q.push(neigh);
        }
    }

    return false;
}

vertex_vec_t reachable_simple_path(const directed_graph &g, const vertex_t &start)
{
    fast_map<vertex_t, bool> visited;
	for(auto v: g.get_vertices())
		visited[v] = false;
	queue<vertex_t> q;
	q.push(start); visited[start] = true;

	vertex_vec_t ret;
	while(!q.empty())
	{
		auto cur = q.front(); q.pop();
		for(const auto & neigh : g.successors(cur))
		{
			if(g.contains_edge(neigh, cur)) //ignore 2way edges
				continue;
			if(visited[neigh])
				continue;

			visited[neigh] = true;
			ret.push_back(neigh);
			q.push(neigh);
		}
	}
	return ret;
}

bool is_on_one_way_cycle(const directed_graph & graph, directed_graph::vertex_t a, directed_graph::vertex_t b)
{
	return exist_simple_path(graph, a, b) && exist_simple_path(graph, b, a);
}
