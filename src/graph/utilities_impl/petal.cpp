#include "../utils.hpp"
#include <limits>
#include "../flows/ford_fulkerson.hpp"
#include "../../fast_hash_table.hpp"

using namespace std;
using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;


ford_fulkerson create_network(const directed_graph& graph, directed_graph::vertex_t v)
{
	fast_map<vertex_t, vertex_t> forward_map;
	vertex_t N = 0;
	for(vertex_t u: graph.get_vertices())
	{
		forward_map[u] = N++;
	}

#define IN(v) (2*(v))
#define OUT(v) (2*(v)+1)
	
	ford_fulkerson ff(2 * N, OUT(forward_map[v]), IN(forward_map[v]));
	for(vertex_t u: graph.get_vertices())
	{
		for(vertex_t w: graph.successors(u))
		{
			ff.add_edge(OUT(forward_map[w]), IN(forward_map[u]), 1);
		}
		if(u != v)
		{
			ff.add_edge(IN(forward_map[u]), OUT(forward_map[u]), 1);
		}
	}
	return ff;
}

size_t petal(const directed_graph& graph, directed_graph::vertex_t v)
{
	ford_fulkerson ff = create_network(graph, v);
	return ff.max_flow();
}

pair<bool, size_t> petal(const directed_graph& graph, directed_graph::vertex_t v, size_t k)
{
	ford_fulkerson ff = create_network(graph, v);
	return ff.max_flow(k);
}



