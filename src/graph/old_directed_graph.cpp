#include "old_directed_graph.hpp"

#include <string>
#include <cassert>

#ifdef XXXXX

directed_graph::directed_graph(size_t n)
{
	for(size_t v = 0; v < n; ++v)
		add_vertex(v);
}

size_t directed_graph::get_vertices_count() const
{
	return get_vertices().size();
}

size_t directed_graph::get_edges_count() const
{
	return m;
}

bool directed_graph::contains_edge(vertex_t u, vertex_t v) const
{
	return contains_vertex(u) && successors(u).find(v) != successors(u).end();
}

void directed_graph::add_edge(vertex_t u, vertex_t v)
{
	if(contains_edge(u, v))
		return;

	++m;
	successors(u).insert(v);
	predecessors(v).insert(u);
}

void directed_graph::remove_edge(vertex_t u, vertex_t v)
{
	if(!contains_edge(u, v))
		return;

	--m;
	successors(u).erase(v);
	predecessors(v).erase(u);
}

directed_graph directed_graph::reversed_edges() const
{
	directed_graph g = *this;
	g.reverse_edges();
	return g;
}
void directed_graph::reverse_edges()
{
	swap(succs, preds);
}
const directed_graph::vertex_set_t& directed_graph::get_vertices() const
{
	return vertices;
}
bool directed_graph::contains_vertex(vertex_t v) const
{
	return vertices.find(v) != vertices.end();
}
void directed_graph::add_vertex(vertex_t v)
{
	assert(!contains_vertex(v) && "attempted to add existing vertex");
	vertices.insert(v);
	succs[v];
	preds[v];
	next=std::max(v+1,next);
}
void directed_graph::remove_vertex(vertex_t v)
{
	assert(contains_vertex(v) && "attempted to remove non-existent vertex");
	for(vertex_t p: predecessors(v))
	{
		successors(p).erase(v);
		m--;
	}
	for(vertex_t s: successors(v))
	{
		predecessors(s).erase(v);
		m--;
	}
	succs.erase(v);
	preds.erase(v);
	vertices.erase(v);
}
void directed_graph::rename_vertex(vertex_t old_name, vertex_t new_name)
{
	assert(contains_vertex(old_name) && "tried to rename non-existent vertex");
	assert(!contains_vertex(new_name) && "tried to rename to existent vertex");
	add_vertex(new_name);
	for(vertex_t succ: successors(old_name))
	{
		add_edge(new_name, succ);
	}
	for(vertex_t pred: predecessors(old_name))
	{
		add_edge(pred, new_name);
	}
	remove_vertex(old_name);
}

void directed_graph::contract_edge_t(vertex_t u, vertex_t v)
{
	assert(contains_edge(u, v) && "tried to contract non-existent edge");
	assert(u != v && "tried to contract self-loop");
	remove_edge(u, v);
	for(vertex_t succ: successors(v))
	{
		add_edge(u, succ);
	}
	for(vertex_t pred: predecessors(v))
	{
		add_edge(pred, u);
	}
	remove_vertex(v);
}

void directed_graph::contract_edge_h(vertex_t u, vertex_t v)
{
	assert(contains_edge(u, v) && "tried to contract non-existent edge");
	assert(u != v && "tried to contract self-loop");
	remove_edge(u, v);
	for(vertex_t succ: successors(u))
	{
		add_edge(v, succ);
	}
	for(vertex_t pred: predecessors(u))
	{
		add_edge(pred, v);
	}
	remove_vertex(u);
}


directed_graph::adj_list_t& directed_graph::successors(vertex_t v)
{
	return const_cast<directed_graph::adj_list_t&>(std::as_const(*this).successors(v));
}

const directed_graph::adj_list_t& directed_graph::successors(vertex_t v) const
{
	assert(succs.find(v) != succs.end() && "asked for successors of non-existent vertex");
	return succs.find(v)->second;
}

directed_graph::adj_list_t& directed_graph::predecessors(vertex_t v)
{
	return const_cast<directed_graph::adj_list_t&>(std::as_const(*this).predecessors(v));
}

const directed_graph::adj_list_t& directed_graph::predecessors(vertex_t v) const
{
	assert(preds.find(v) != preds.end() && "asked for predecessor of non-existent vertex");
	return preds.find(v)->second;
}

directed_graph::vertex_set_t directed_graph::neighbours(directed_graph::vertex_t v) const
{
	directed_graph::vertex_set_t ret(predecessors(v));
	for (auto u : successors(v)) ret.insert(u);
	return ret;
}

size_t directed_graph::out_degree(vertex_t v) const
{
	return successors(v).size();
}
size_t directed_graph::in_degree(vertex_t v) const
{
	return predecessors(v).size();
}

directed_graph::vertex_vec_t directed_graph::get_vertices_vec() const
{
	vertex_vec_t res;
	for(vertex_t v: get_vertices())
	{
		res.push_back(v);
	}
	return res;
}
directed_graph::vertex_t directed_graph::add_new_vertex()
{
	vertex_t ret = next;
	add_vertex(next);
	return ret;
}

#endif
