#pragma once

#include <iostream>

#include "../io/io.hpp"
#include "../io/resultPrinter.hpp"
#include "../graph/utils.hpp"
#include <cassert>

template<typename Solver, typename... Args>
int go(std::ostream & os = std::cout, Args&& ... args)
{
	directed_graph graph = load_graph<directed_graph>(std::cin);
	Solver solver(std::forward<Args>(args)...);
	auto dfvs = calculate_on_components(graph, [&solver](const directed_graph & component){return solver.solve_dfvs(component);});

#ifdef __DEBUG__
	os << dfvs.size() << std::endl;
	for(directed_graph::vertex_t v: dfvs)
		os << v << std::endl;
#else
	output_solution<directed_graph>(os, dfvs);
#endif

	for(auto v : dfvs){
		graph.remove_vertex(v);
	}
	assert(is_DAG(graph));
	return 0;
}