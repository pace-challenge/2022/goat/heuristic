#include "vertexCutSolver.hpp"
#include "vertexBranchFinder.hpp"
#include "../graph/utils.hpp"
#include <vector>
#include <utility>
#include <algorithm>
#include <iostream>
using namespace std;
using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_t = directed_graph::vertex_t;

template <typename Container, typename Iterator>
bool contains(const Container &container, Iterator start, Iterator end)
{
	while (start != end)
	{
		if (std::find(container.begin(), container.end(), *start) == container.end())
			return false;
		++start;
	}
	return true;
}

bool cyclebranch_solver::handleOneCut_2Cut(const directed_graph &graph, directed_graph::vertex_vec_t &solution,
										   const directed_graph::vertex_vec_t &componentV, const directed_graph::vertex_vec_t &vertexCut,
										   directed_graph::vertex_vec_t &toDeleteFromCut, bool parametrized, int &budget,
										   const directed_graph::vertex_vec_t &originalSolution)
{
	(void) parametrized; //warning bypassing

	if (vertexCut.size() != 2)
		return false;

	int ub = originalSolution.size();
	// first which vertex was left out, second the solution of the whole subgraph without the left out vertex
	vector<pair<vertex_t, vertex_vec_t>> localSolutions;

	for (int i = 0; i < 2; i++)
	{
		vertex_t u = vertexCut[i], v = vertexCut[1 - i];
		vertex_t leftOut = u;

		vertex_vec_t componentV_without_u = componentV;
		componentV_without_u.push_back(v);
		directed_graph A = graph.induced_subgraph(componentV_without_u.begin(), componentV_without_u.end());
		vertex_vec_t A_solution;

		// chooseSolveStrategy(A, A_solution);
		// if((int)A_solution.size() == ub - 1)
		if (solve_with_parameter_k(A, A_solution, ub - 1))
			localSolutions.push_back({leftOut, A_solution});
	}

	if (localSolutions.size() == 0)
	{
		// todo: find out why does this not work >:(
		// solver doesnt return optimal solution size if i uncomment this
		return false;

		// know that neither of them are not in solution, take original solution that handles everything
		solution.insert(solution.end(), originalSolution.begin(), originalSolution.end());
		budget -= ub;
		return true;
	}
	else if (localSolutions.size() == 1)
	{
		// know that one of the 2 will never be in solution, if someone else adds it then its their business
		// from the view of this component, it is optimal to take the one that uses the vertexCut as much as possible
		auto [usedVertex, componentSolution] = localSolutions.front();
		solution.insert(solution.end(), componentSolution.begin(), componentSolution.end());
		solution.push_back(usedVertex);
		budget -= componentSolution.size() + 1;
		toDeleteFromCut = {usedVertex};

		return true;
	}
	else
	{
		// know that leftOut vertex is in one of its solutions but both of them are not in solution
		// having 1 in solution only is useless, dont know which one to use
		return false;
	}
}

bool cyclebranch_solver::handleOneCut(const directed_graph &graph, vertex_vec_t &solution,
									  const vertex_vec_t &componentV, const vertex_vec_t &vertexCut,
									  vertex_vec_t &toDeleteFromCut,
									  bool parametrized, int &budget)
{
	vertex_vec_t componentV_with_cut = componentV;
	componentV_with_cut.insert(componentV_with_cut.end(), vertexCut.begin(), vertexCut.end());
	directed_graph A_cut = graph.induced_subgraph(componentV_with_cut.begin(), componentV_with_cut.end());
	if (strongly_connected_components(A_cut).size() > 1)
		return false;

	vertex_vec_t A_cut_solution;

	if (!parametrized)
	{
		chooseSolveStrategy(A_cut, A_cut_solution);
	}
	else if (!solve_with_parameter(A_cut, A_cut_solution, budget))
	{
		budget = -1;
		return true;
	}

	size_t k = A_cut_solution.size();
	if (k == 0)
		return true;

	if (contains(A_cut_solution, vertexCut.begin(), vertexCut.end()))
	{
		solution.insert(solution.end(), A_cut_solution.begin(), A_cut_solution.end());
		budget -= k;
		toDeleteFromCut = vertexCut;
		return true;
	}

	directed_graph A = graph.induced_subgraph(componentV.begin(), componentV.end());
	vertex_vec_t A_solution;

	// chooseSolveStrategy(A, A_solution);
	// if(A_solution.size() + vertexCut.size() == (size_t)k)
	if (solve_with_parameter_k(A, A_solution, k - vertexCut.size()))
	{
		solution.insert(solution.end(), A_solution.begin(), A_solution.end());
		solution.insert(solution.end(), vertexCut.begin(), vertexCut.end());
		budget -= k;
		toDeleteFromCut = vertexCut;
		return true;
	}

	if (vertexCut.size() == 1)
	{
		solution.insert(solution.end(), A_cut_solution.begin(), A_cut_solution.end());
		budget -= k;
		return true;
	}

	if (handleOneCut_2Cut(graph, solution, componentV, vertexCut, toDeleteFromCut, parametrized, budget, A_cut_solution))
		return true;

	return false;
}

bool shouldTakeCut(const directed_graph &copy, const directed_graph::vertex_vec_t &cut)
{
	for (auto v : cut)
		if (!copy.contains_vertex(v))
			return false;
	return true;
}

cyclebranch_solver::branch_code cyclebranch_solver::solve_with_VertexCut(bool weaklyConnected,
																		 const directed_graph &graph,
																		 directed_graph::vertex_vec_t &solution,
																		 int cutSize, int maxComponentSize, bool parametrized, int ub)
{
	using CUT = vertex_vec_t;
	using CUT_COMPONENTS = vertex_vec_t;
	vector<pair<CUT, CUT_COMPONENTS>> instances;
	getPotatoInstances(graph, weaklyConnected, cutSize, maxComponentSize, instances);
	if (instances.empty())
		return NO_BRANCHING;

	directed_graph copy = graph;
	int budget = ub;
	bool applied = false;
	for (auto [cut, componentV] : instances)
	{
		assert((int)cut.size() == cutSize);
		if (!shouldTakeCut(copy, cut))
			continue;

		componentV.erase(
			remove_if(componentV.begin(), componentV.end(),
					  [&copy](const vertex_t &v)
					  { return !copy.contains_vertex(v); }),
			componentV.end());

		if (componentV.empty())
			continue;

		vertex_vec_t toDeleteFromCut;
		if (!handleOneCut(copy, solution, componentV, cut, toDeleteFromCut, parametrized, budget))
			continue;

		applied = true;
		if (budget < 0)
		{
			assert(parametrized); //todo: wrap in define or catch if messed up implementation
			return NO_SOLUTION;
		}

		for (auto v : componentV)
			copy.remove_vertex(v);
		for (auto v : toDeleteFromCut)
			copy.remove_vertex(v);
		if (!toDeleteFromCut.empty())
			break;
	}
	if (!applied)
		return NO_BRANCHING;

	if (copy.get_vertices_count() > 100) //todo: remove print
		cerr << "starting over again " << copy.get_vertices_count() << endl;
	if (parametrized)
	{
		return solve_with_parameter_k(copy, solution, budget) ? SOLUTION_FOUND : NO_SOLUTION;
	}
	else
	{
		solve_with_SCC(copy, solution);
		return SOLUTION_FOUND;
	}
}

//-------------------------------------------------------------------------

cyclebranch_solver::branch_code cyclebranch_solver::solve_with_weak_cut(const directed_graph &graph,
																		directed_graph::vertex_vec_t &solution,
																		int cutSize, int maxComponentSize, bool parametrized, int ub)
{
	if (cutSize <= 2)
		return solve_with_VertexCut(true, graph, solution, cutSize, maxComponentSize, parametrized, ub);
	return NO_BRANCHING;
}

cyclebranch_solver::branch_code cyclebranch_solver::solve_with_weak_cut(const directed_graph &graph,
																		vertex_vec_t &solution,
																		int maxComponentSize, bool parametrized, int ub)
{
	int maxVertexCutSize = 2;

	for (int cutSize = 1; cutSize <= maxVertexCutSize; cutSize++)
	{
		auto retCode = solve_with_weak_cut(graph, solution, cutSize, maxComponentSize, parametrized, ub);
		if (retCode == SOLUTION_FOUND || retCode == NO_SOLUTION)
			return retCode;
	}
	return NO_BRANCHING;
}

//-------------------------------------------------------------------------

cyclebranch_solver::branch_code cyclebranch_solver::solve_with_strong_cut(const directed_graph &graph,
																		  directed_graph::vertex_vec_t &solution,
																		  int cutSize, int maxComponentSize, bool parametrized, int ub)
{
	if (cutSize == 1)
		return solve_with_VertexCut(false, graph, solution, cutSize, maxComponentSize, parametrized, ub);

	return NO_BRANCHING;
}

cyclebranch_solver::branch_code cyclebranch_solver::solve_with_strong_cut(const directed_graph &graph, vertex_vec_t &solution,
																		  int maxComponentSize, bool parametrized, int ub)
{
	int maxVertexCutSize = 1;
	for (int cutSize = 1; cutSize <= maxVertexCutSize; cutSize++)
	{
		auto retCode = solve_with_strong_cut(graph, solution, cutSize, maxComponentSize, parametrized, ub);
		if (retCode == SOLUTION_FOUND || retCode == NO_SOLUTION)
			return retCode;
	}
	return NO_BRANCHING;
}

//-------------------------------------------------------------------------

cyclebranch_solver::branch_code cyclebranch_solver::solve_with_vertex_cut2(const directed_graph &graph,
																		   vertex_vec_t &solution,
																		   int maxComponentSize,
																		   bool parametrized, int ub)
{
	auto weak_retCode = solve_with_weak_cut(graph, solution, maxComponentSize, parametrized, ub);
	if (weak_retCode == SOLUTION_FOUND || weak_retCode == NO_SOLUTION)
		return weak_retCode;

	auto strong_retCode = solve_with_strong_cut(graph, solution, maxComponentSize, parametrized, ub);
	if (strong_retCode == SOLUTION_FOUND || strong_retCode == NO_SOLUTION)
		return strong_retCode;

	return NO_BRANCHING;
}

//-------------------------------------------------------------------------

cyclebranch_solver::branch_code cyclebranch_solver::solve_with_vertex_cut(const directed_graph &graph,
																		  vertex_vec_t &solution)
{
	int maxComponentSize = graph.get_vertices_count();
	int trivialUB = graph.get_vertices_count();
	vertex_vec_t localSolution;
	auto retCode = solve_with_vertex_cut2(graph, localSolution, maxComponentSize, false, trivialUB);
	assert(retCode != NO_SOLUTION);
	if (retCode != NO_BRANCHING)
		solution.insert(solution.end(), localSolution.begin(), localSolution.end());

	return retCode;
}

cyclebranch_solver::branch_code cyclebranch_solver::solve_with_vertex_cut(const directed_graph &graph,
																		  vertex_vec_t &solution, int k)
{
	int maxComponentSize = graph.get_vertices_count();
	auto retCode = solve_with_vertex_cut2(graph, solution, maxComponentSize, true, k);
	return retCode;
}