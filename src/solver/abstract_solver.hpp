#pragma once
#include "../graph/directed_graph.hpp"

class abstract_solver
{
 public:
	virtual ~abstract_solver() = default;
	virtual typename directed_graph::vertex_vec_t solve_dfvs(const directed_graph& g) = 0;
	virtual std::string description() const = 0;
};






