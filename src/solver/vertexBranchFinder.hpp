#pragma once

#include "../graph/directed_graph.hpp"
#include <vector>
#include <utility>

/*
* Returns vertex with highest two way degree, -1 if it does not exists
*/
directed_graph::vertex_t best_VC_branching_vertex(const directed_graph & graph);

/*
 * Finds cycle that has the least vertices that we dont know that they are not in the solution
 */
directed_graph::vertex_vec_t get_best_cycle(const directed_graph & graph);

template<bool weaklyConnected>
bool shouldIgnoreCut(const directed_graph & g, const directed_graph::vertex_vec_t & cut);

/**
 * @brief finds cuts of size {@code cutSize} in g and returns components that are of size <= vertexCountLimit, and the cut. Also, the cut has to create a twoway clique
 * 
 * @details first finds all cuts, then for each remove it from graph and it splits into smaller weakly connected components.
 * returns sorted list of such components by their size
 * 
 * @param weaklyConnected: uses weakly connected component or strongly connected component
 * @param instances: return parameter
 * @param cutSize: size of vertex cut that will be found
 */
void getPotatoInstances(const directed_graph & g, bool weaklyConnected, int cutSize, int vertexCountLimit,
    std::vector<std::pair<directed_graph::vertex_vec_t, directed_graph::vertex_vec_t>> &instances);