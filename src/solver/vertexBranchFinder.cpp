#include "vertexBranchFinder.hpp"
#include "../graph/utils.hpp"
#include "../fast_hash_table.hpp"

#include <utility>
#include <algorithm>

using namespace std;

using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;

vertex_t best_VC_branching_vertex(const directed_graph & graph){
    auto two_cycles = find_all2Cycles(graph);
    fast_map<directed_graph::vertex_t, int> counts;
    if(two_cycles.empty()){
        return -1;
    }
    int max_count = -1;
    directed_graph::vertex_t to_branch = -1;
    for(auto & cycle : two_cycles){
        for(const directed_graph::vertex_t v : cycle){
            if(counts.find(v) == counts.end()){
                counts[v] = 0;
            }
            ++counts[v];
            if(counts[v] > max_count){
                max_count = counts[v];
                to_branch = v;
            }
        }
    }
    return to_branch;
}

vertex_vec_t get_best_cycle(const directed_graph & graph){
    // todo faster - search for the shortest cycle but dont increase count when not_in_solution vertex is found
    auto cycles = find_shortest_cycle_foreach_v(graph); // todo faster
    if(cycles.size() == 0){
        return {};
    }
    // cycle, adjusted_size, sum of degrees
    vector<tuple<vertex_vec_t, int, int>> labeled_cycles;
    for(const auto & cycle : cycles){
        labeled_cycles.push_back({cycle, cycle.size(), sum_degrees(cycle, graph)});
    }
    // we want to branch on the smallest cycle where the sum of degreees is the highest
    sort(labeled_cycles.begin(), labeled_cycles.end(), [](const tuple<vertex_vec_t, int, int> & a, const tuple<vertex_vec_t, int, int> & b){
        // compare size first
        if(get<1>(a) != get<1>(b)){
            return get<1>(a) < get<1>(b);
        }
        // then sum degrees
        return get<2>(a) > get<2>(b);
    });
    return get<0>(labeled_cycles[0]);
}


//------------------------------------------------------------------------------------------
template<>
bool shouldIgnoreCut<true>(const directed_graph & g, const directed_graph::vertex_vec_t & cut)
{
    if(cut.size() == 1)
        return false;

    if(is_clique(g, directed_graph::vertex_set_t(cut.begin(), cut.end())))
        return false;

    if(cut.size() == 2)
    {
        // if(!g.contains_edge(cut[0], cut[1]) && !g.contains_edge(cut[1], cut[0]))
        {
            if(!is_on_one_way_cycle(g, cut[0], cut[1])){
                return false;
            }
            //todo: can something be said if only one way edges are used between the cuts?
            //or maybe this case cant happen
        }
        //todo: check if for one way edge something can be done
    }
    return true;
}

template<>
bool shouldIgnoreCut<false>(const directed_graph & g, const directed_graph::vertex_vec_t & cut)
{
    if(cut.size() == 1)
    {
        if(incident_with_only_twoWay_edge(g, cut.front()))
            return false;
        if(!exist_simple_path(g, cut.front(), cut.front()))
            return false;
    }

    return true;
}

template<bool weaklyConnected>
void getPotatoInstances2(const directed_graph & g, int cutSize, int vertexCountLimit,
    std::vector<std::pair<directed_graph::vertex_vec_t, directed_graph::vertex_vec_t>> &instances)
{
    using P_vec_vec = std::pair<directed_graph::vertex_vec_t, directed_graph::vertex_vec_t>;
    int cutsLeft = 0;
	auto sccs = strongly_connected_components(g);
	for(const auto & vec : sccs)
	{
		directed_graph component = g.induced_subgraph(vec.begin(), vec.end());
		vector<directed_graph::vertex_vec_t> allCuts = getVertexCut<weaklyConnected>(component, cutSize);
		for(const auto & cut : allCuts)
		{
			if(shouldIgnoreCut<weaklyConnected>(component, cut))
				continue;
			cutsLeft++;
			auto copy = component;
			for(auto v : cut)
				copy.remove_vertex(v);
			
			auto components = weakly_connected_components(copy);
			
			for(const auto & componentV : components)
			{
				if((int)componentV.size() <= vertexCountLimit)
					instances.push_back({cut, componentV});            
			}
		}
	}
    sort(instances.begin(), instances.end(),
        [](const P_vec_vec & a, const P_vec_vec & b)
        {
            return a.second.size() < b.second.size();
        }
    );
}

void getPotatoInstances(const directed_graph & g, bool weaklyConnected, int cutSize, int vertexCountLimit,
    std::vector<std::pair<directed_graph::vertex_vec_t, directed_graph::vertex_vec_t>> &instances)
{
    if(weaklyConnected)
        getPotatoInstances2<true>(g, cutSize, vertexCountLimit, instances);
    else
        getPotatoInstances2<false>(g, cutSize, vertexCountLimit, instances);
}