#include "bf.hpp"
#include "../graph/utils.hpp"

using namespace std;

bool is_in_set(int v, uint64_t set_mask)
{
	return (1ULL << v) & set_mask;
}

size_t set_size(uint64_t set_mask)
{
	return __builtin_popcountl(set_mask);
}

typename directed_graph::vertex_vec_t bf_solver::solve_dfvs(const directed_graph& graph)
{
	if(graph.get_vertices_count() > 50)
	{
		throw runtime_error("Graph too big for bruteforce");
	}
	uint64_t best_set = ~0;
	directed_graph::vertex_vec_t vertices = graph.get_vertices_vec();
	
	for(uint64_t set_mask = 0; set_mask < (1ULL << graph.get_vertices_count()); ++set_mask)
	{
		directed_graph graph_copy = graph;
		for(size_t i = 0; i < graph.get_vertices_count(); ++i)
		{
			if(is_in_set(i, set_mask))
			{
				graph_copy.remove_vertex(vertices[i]);
			}
		}
		if(is_DAG(graph_copy) && set_size(set_mask) < set_size(best_set))
		{
			best_set = set_mask;
		}
	}
	
	directed_graph::vertex_vec_t res;
	for(size_t i = 0; i < graph.get_vertices_count(); ++i)
	{
		if(is_in_set(i, best_set))
		{
			res.push_back(vertices[i]);
		}
	}
	return res;
}
std::string bf_solver::description() const
{
	return "bruteforce";
}
