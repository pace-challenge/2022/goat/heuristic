#pragma once

#include "../graph/directed_graph.hpp"
#include "abstract_solver.hpp"


class bf_solver : public abstract_solver
{
 public:
	typename directed_graph::vertex_vec_t solve_dfvs(const directed_graph& graph) override;
	std::string description() const override;
};