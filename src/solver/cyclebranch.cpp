#include "cyclebranch.hpp"
#include "../graph/utils.hpp"
#include "../graph/directed_graph.hpp"
#include "../reduction_rules/combinations.hpp"
#include "../reduction_rules/reverse_rule.hpp"
#include "../lowerbounds/cyclepacking.hpp"
#include "../upperbounds/maxdegree.hpp"
#include "../upperbounds/generic_ub.hpp"

#include "vertexBranchFinder.hpp"

#include <queue>
#include <utility>
#include <algorithm>
#include <iostream>
#include <numeric>

using namespace std;

using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;

bool cyclebranch_solver::handle_multiple_components(const directed_graph & graph,
													const std::vector<vertex_vec_t> &sccs,
													vertex_vec_t & deleted, int k)
{
    vector<directed_graph> components;
    // order components by solution size, only by solution size then by vertices count
    // priority queue is max heap - we use a > b instead
    auto cmp = [&components](const pair<int, int> & a, const pair<int, int> & b){
        if(a.second != b.second){
            return a.second > b.second;
        }
        return components[a.first].get_vertices_count() > components[b.first].get_vertices_count(); 
    };
    // first component color, second current solution size
    priority_queue<pair<int, int>,std::vector<pair<int, int>>, decltype(cmp)> q(cmp);  // solve first the cases with smallest parameter
    int remaining_k = k;
    for(const auto & vertices : sccs){
        directed_graph subgraph = graph.induced_subgraph(vertices.begin(), vertices.end());
		
		int oldSolSize = deleted.size();
		int ret_code = solve_with_VertexCover(subgraph, deleted, remaining_k);
		if(ret_code == NO_SOLUTION){
			return false;
		}
		else if(ret_code == SOLUTION_FOUND){
            remaining_k -= (deleted.size() - oldSolSize);
			if(remaining_k < 0)
                return false;
		}
        else
        {
            components.push_back(subgraph);
            int lb = getLB_fast(components.back());
            remaining_k -= lb;
            if(remaining_k < 0)
                return false;

            q.push({components.size() - 1, lb});
        }
    }

    vertex_vec_t all_components_solution;
    while(remaining_k >= 0 && !q.empty()){
        auto[component_id, parameter] = q.top();
        q.pop();

        vertex_vec_t one_component_solution;

        if(chooseBranchStrategy(components[component_id], one_component_solution, parameter)){
            all_components_solution.insert(all_components_solution.end(), one_component_solution.begin(), one_component_solution.end());
        } else{
            q.push({component_id, parameter +1});
            remaining_k--;
        }
    }
    if(q.empty() && remaining_k >= 0){
        deleted.insert(deleted.end(), all_components_solution.begin(), all_components_solution.end());
        return true;
    }
    return false;
}

bool cyclebranch_solver::branch_wrapper(const directed_graph & graph, vertex_vec_t & deleted, int k,
                                        const vertex_vec_t & to_delete)
{
    int k_decrease = to_delete.size();
    int new_k = k - k_decrease;
    if(new_k < 0)
        return false;

    vertex_vec_t branch_solution;
    directed_graph copy = graph;
    for(vertex_t w : to_delete){
        copy.remove_vertex(w);
        branch_solution.push_back(w);
    }

	if(!chooseBranchStrategy(copy, branch_solution, new_k))
		return false;

	deleted.insert(deleted.end(), branch_solution.begin(), branch_solution.end());
    return true;
}

//------------------------------------------------------------------------------------

bool handleBranchSolution(vertex_vec_t &totalSolution, vertex_vec_t&localSolution,
						reverse_rules_stack<directed_graph> & reverse_rules,
						bool retCode)
{
	if(!retCode)
		return false;
	resolveReverseRules(localSolution, reverse_rules);
	totalSolution.insert(totalSolution.end(), localSolution.begin(), localSolution.end());
	return true;
}

bool cyclebranch_solver::chooseBranchStrategy(const directed_graph & g,
                                			  vertex_vec_t & deleted,
											  int k)
{
    if(k < 0){
		return false;
	}
	if(is_DAG(g)){
		return true;
	}

	directed_graph graph = g;
	vertex_vec_t localSolution; reverse_rules_stack<directed_graph> local_rr;
	k -= reduce_all(graph, localSolution, k, local_rr);

	if(k < 0 || getLB_fast(graph) > k){
		return false;
	}
	if(is_DAG(graph)){
		return handleBranchSolution(deleted, localSolution, local_rr, true);
	}

	// auto UB_return_code = solve_with_UB(graph, localSolution, k);
	// if(UB_return_code == NO_SOLUTION || UB_return_code == SOLUTION_FOUND){
	// 	return handleBranchSolution(deleted, localSolution, local_rr, UB_return_code == SOLUTION_FOUND);
	// }

	// auto cut_return_code = solve_with_vertex_cut(graph, localSolution, k);
	// if(cut_return_code == NO_SOLUTION || cut_return_code == SOLUTION_FOUND){
	// 	return handleBranchSolution(deleted, localSolution, local_rr, cut_return_code == SOLUTION_FOUND);
	// }

	auto sccs = strongly_connected_components(graph);
	if(sccs.size() > 1){
		bool ret_code = handle_multiple_components(graph, sccs, localSolution, k);
		return handleBranchSolution(deleted, localSolution, local_rr, ret_code);
	}

	auto VC_return_code = solve_with_VertexCover(graph, localSolution, k);
	if(VC_return_code == NO_SOLUTION || VC_return_code == SOLUTION_FOUND){
        return handleBranchSolution(deleted, localSolution, local_rr, VC_return_code == SOLUTION_FOUND);
    } 

    auto return_code = branch_on_2_cycles(graph, localSolution, k);
    if(return_code == NO_SOLUTION || return_code == SOLUTION_FOUND){
        return handleBranchSolution(deleted, localSolution, local_rr, return_code == SOLUTION_FOUND);
    } 

    bool best_cycle_ret_code = branch_on_best_cycle(graph, localSolution, k);
	return handleBranchSolution(deleted, localSolution, local_rr, best_cycle_ret_code);
}

cyclebranch_solver::branch_code cyclebranch_solver::branch_on_2_cycles(const directed_graph & graph,
                            										  vertex_vec_t & deleted,
																	  int k)
{
    directed_graph::vertex_t to_branch = best_VC_branching_vertex(graph);
    if(to_branch == -1){
        return NO_BRANCHING;
    }

    // either to_branch is in the solution
    if(branch_wrapper(graph, deleted, k, to_branch)){
        return SOLUTION_FOUND;
    }

    // or all two way neighbours are in the solution
    vertex_vec_t to_del;
    for(vertex_t w : graph.successors(to_branch)){
        if(graph.contains_edge(w, to_branch)){
            to_del.push_back(w);
        }
    }
    #ifdef __DEBUG__
    assert(!to_del.empty());
    #endif

    directed_graph g = graph;
    notInSolution(g, to_branch);

    if(branch_wrapper(g, deleted, k, to_del)){
        return SOLUTION_FOUND;
    }

    return NO_SOLUTION;
}

cyclebranch_solver::branch_code cyclebranch_solver::solve_with_UB(const directed_graph & graph, directed_graph::vertex_vec_t & deleted, int k)
{
	auto dfvs = getUB_solution(graph);
	if((int)dfvs.size() > k)
		return NO_BRANCHING;
	
	deleted.insert(deleted.end(), dfvs.begin(), dfvs.end());
	return SOLUTION_FOUND;
}

bool cyclebranch_solver::branch_on_best_cycle(const directed_graph & graph, directed_graph::vertex_vec_t & deleted, int k)
{
    auto smallest_cycle = get_best_cycle(graph);
    if(smallest_cycle.size() == 0){
        return true;
    } else if(k <= 0){
        return false;
    }

    directed_graph g = graph;

    for(directed_graph::vertex_t v : smallest_cycle){
        if(branch_wrapper(g, deleted, k, v)){
            return true;
        }
        else{
            notInSolution(g, v);
        }
    }
    return false;
}

//------------------------------------------------------------------------------------

int cyclebranch_solver::getLB_slow(const directed_graph & graph) const
{
	vector<int> lbs;
	lbs.push_back(getLB_fast(graph));
	lbs.push_back(lpvc_plus_pack_cycles_lb(graph));
	lbs.push_back(pack_cliques_lb_slow(graph));
	return *std::max_element(lbs.begin(), lbs.end());
}

int cyclebranch_solver::getLB_fast(const directed_graph & graph) const
{
	vector<int> lbs;
	lbs.push_back(pack_cliques_lb(graph));
	return *std::max_element(lbs.begin(), lbs.end());
}

vertex_vec_t cyclebranch_solver::getUB_solution(const directed_graph & graph) const
{
	vector<pair<int, vertex_vec_t>> ubs;
	{
		auto dfvs = max_degree_ub().solve_dfvs(graph);
		ubs.push_back({dfvs.size(), dfvs});
	}

	{
		auto max_degree_sum = [](const directed_graph & g, const vertex_t & v){return sum_degrees({v}, g);};
		generic_ub<decltype(max_degree_sum)> gub(max_degree_sum);
		auto dfvs = calculate_on_components(graph, [&gub](const directed_graph & component){return gub.solve_dfvs(component);});
		ubs.push_back({dfvs.size(), dfvs});
	}
	
	return std::min_element(ubs.begin(), ubs.end())->second;
}

int cyclebranch_solver::getUB(const directed_graph & graph) const
{
	return getUB_solution(graph).size();
}

bool cyclebranch_solver::solve_with_parameter(const directed_graph& graph, directed_graph::vertex_vec_t & solution) {
    directed_graph copy = graph;
    reverse_rules_stack<directed_graph> rr;
    reduce_all(copy, solution, rr);

    int LB = getLB_slow(copy), UB = getUB(copy);
    assert(solve_with_parameter(copy, solution, LB, UB));
    resolveReverseRules(solution, rr);
    return true;
}

bool cyclebranch_solver::solve_with_parameter(const directed_graph& graph, directed_graph::vertex_vec_t & solution, int ub) {
	directed_graph copy = graph;
	vertex_vec_t localSolution; reverse_rules_stack<directed_graph> rr;
	ub -= reduce_all(copy, localSolution, rr);

    int LB = getLB_slow(copy), customUB = getUB(copy);
	int finalUB = min(customUB, ub);
	if(!solve_with_parameter_bsearch(copy, localSolution, LB, finalUB)) //todo: change
		return false;

	resolveReverseRules(localSolution, rr);
	solution.insert(solution.end(), localSolution.begin(), localSolution.end());
    return true;
}

bool cyclebranch_solver::solve_with_parameter(const directed_graph& graph, directed_graph::vertex_vec_t & solution, int lb, int ub) {
    if(ub < 0) return false;

    return solve_with_parameter_bsearch(graph, solution, lb, ub, true);
}

//------------------------------------------------------------------------------------

bool cyclebranch_solver::solve_with_parameter_linear(const directed_graph& graph, directed_graph::vertex_vec_t & solution, int lb, int ub) {
	for(int k = lb; k <= ub; k++) {
		if(solve_with_parameter_k(graph, solution, k))
			return true;
	}
	return false;
}

bool cyclebranch_solver::solve_with_parameter_k(const directed_graph& graph, directed_graph::vertex_vec_t & solution, int k) {
	directed_graph::vertex_vec_t deleted_vertices;
	if(!chooseBranchStrategy(graph, deleted_vertices, k))
		return false;

	solution.insert(solution.end(), deleted_vertices.begin(), deleted_vertices.end());
	return true;
}

bool cyclebranch_solver::solve_with_parameter_bsearch(const directed_graph& graph, directed_graph::vertex_vec_t & solution, int lb, int ub, bool print) {
	directed_graph::vertex_vec_t total_solution;
	int first = lb, last = ub+1, count = last - first;
	/*
	copies https://en.cppreference.com/w/cpp/algorithm/upper_bound 2nd version
	*/
	//todo: cleanup this print
	if(print)
		cerr << "param solve " << graph.get_vertices_count() << ", lb = " << lb << ", ub = " << ub << endl;
	while(count > 0)
	{
		int it = first, step = count/ 2;
		it += step;
		directed_graph::vertex_vec_t local_solution;
		if(print)
			cerr << "param solve " << graph.get_vertices_count() << ", k = " << it << ", lb = " << lb << ", ub = " << ub << endl;
		if(!solve_with_parameter_k(graph, local_solution, it)){
			first = ++it, count -= step+1;
		}
		else{
			total_solution = local_solution, count = step;
		}
	}
	if(first >= last)
		return false;

	int finalk = total_solution.size();
	assert(finalk >= lb && finalk <= ub);

	solution.insert(solution.end(), total_solution.begin(), total_solution.end());
	return true;
}

//------------------------------------------------------------------------------------
void cyclebranch_solver::update_danger_param(int reduced_k)
{
	//todo: 
	if(reduced_k > 0)
		dangerous_param += reduced_k;
	else
		dangerous_param += reduced_k*20;
}

cyclebranch_solver::branch_code cyclebranch_solver::applyDangerousReduction(directed_graph graph,
														directed_graph::vertex_vec_t &solution)
{
	if(graph.get_vertices_count() < 120) //too little vertices, just solve it a different way
		return NO_BRANCHING;
	
	//todo: heuristic to count by how much we allow the param to increase
	int allowedIncrease = graph.get_vertices_count()/5;

	cerr << "danger budget " << allowedIncrease << endl;
	int oldV = graph.get_vertices_count();
	cerr << "danger bef " << oldV << endl;

	vertex_vec_t localSolution; reverse_rules_stack<directed_graph> rr;
	int kDiff = dangerous_reduce_all(graph, localSolution, rr, allowedIncrease);
	bool increasedParam = kDiff <= 0;
	if(dangerous_param <= 0 && increasedParam) // no budget and increased k, just end it already
	{
		cerr << "used up budget, increased param" << endl;
		return NO_BRANCHING;
	}

	// applied multiple times and started to have more vertices, abort
	if( graph.applied_danger > 1 && increasedParam)
	{
		cerr << "still same after danger" << endl;
		return NO_BRANCHING;
	}

	update_danger_param(- allowedIncrease); //todo: 
	graph.applied_danger++;

	cerr << "danger aft " << graph.get_vertices_count() << endl;;
	chooseSolveStrategy(graph, localSolution);
	resolveReverseRules(localSolution, rr);
	solution.insert(solution.end(), localSolution.begin(), localSolution.end());

	return SOLUTION_FOUND;
}

//------------------------------------------------------------------------------------

cyclebranch_solver::branch_code cyclebranch_solver::solve_with_VertexCover(const directed_graph &graph,
																  directed_graph::vertex_vec_t &solution)
{
	return solve_with_VertexCover(graph, solution, graph.get_vertices_count());
}

cyclebranch_solver::branch_code cyclebranch_solver::solve_with_VertexCover(const directed_graph &graph,
																  directed_graph::vertex_vec_t &solution,
																  int ub)
{
	return solve_with_VertexCover(graph, solution, 0, ub);
}
				
cyclebranch_solver::branch_code cyclebranch_solver::solve_with_VertexCover(const directed_graph &graph,
																  directed_graph::vertex_vec_t &solution,
																  int lb, int ub)
{
	return NO_BRANCHING; //we dont have an effective VC solver

    if(!hasOnly_twoWay_edges(graph))
		return NO_BRANCHING;
	if(graph.get_vertices_count() > 100)
		cerr << "vc " << graph.get_vertices_count() <<endl;

	vertex_vec_t VCSolution;
	// auto VCSolution = solve_vc(graph);
	
	int solSize = VCSolution.size();
	if(solSize < lb || solSize > ub)
		return NO_SOLUTION;
	
	solution.insert(solution.end(), VCSolution.begin(), VCSolution.end());
	return SOLUTION_FOUND;	
}

//------------------------------------------------------------------------------------

void cyclebranch_solver::chooseSolveStrategy(const directed_graph & graph, directed_graph::vertex_vec_t & solution)
{
	if(is_DAG(graph)) return;

	directed_graph copy = graph;
    reverse_rules_stack<directed_graph> rr;

    int reduced_k = 0;
	reduced_k = reduce_all(copy, solution, rr);
	update_danger_param(reduced_k);

	if(is_DAG(copy))
	{
		resolveReverseRules(solution, rr);
		return;
	}

	if(strongly_connected_components(copy).size() != 1)
	{
		solve_with_SCC(copy, solution);
		resolveReverseRules(solution, rr);
		return;
	}

	if(applyDangerousReduction(copy, solution) == SOLUTION_FOUND)
	{
		resolveReverseRules(solution, rr);
		return;
	}

	if(solve_with_vertex_cut(copy, solution) == SOLUTION_FOUND)
	{
		resolveReverseRules(solution, rr);
		return;
	}

	if(solve_with_VertexCover(copy, solution) == SOLUTION_FOUND)
	{
	    resolveReverseRules(solution, rr);
        return;
	}

	solve_with_parameter(copy, solution);
	resolveReverseRules(solution, rr);
	return;
}

//------------------------------------------------------------------------------------

bool cyclebranch_solver::solve_with_SCC(const directed_graph& graph, directed_graph::vertex_vec_t & result)
{
    vector<directed_graph::vertex_vec_t> sccs = strongly_connected_components(graph);
	if(sccs.size() == 1)
	{
		chooseSolveStrategy(graph, result);
	}
	else
	{
		for(const directed_graph::vertex_vec_t & vertices : sccs){
			directed_graph component = graph.induced_subgraph(vertices.begin(), vertices.end());
			directed_graph::vertex_vec_t component_solution;
			chooseSolveStrategy(component, component_solution);
			result.insert(result.end(), component_solution.begin(), component_solution.end());
		}
	}
    return true;
}

directed_graph::vertex_vec_t cyclebranch_solver::solve_dfvs(const directed_graph& graph) {
    directed_graph reduced = graph;
    directed_graph::vertex_vec_t result;
    reverse_rules_stack<directed_graph> reverseRules;
	
	dangerous_param = 0; //reset for each component!!!
	int reduced_k  = 0;
	reduced_k = reduce_expensive(reduced, result, reverseRules);
	update_danger_param(reduced_k);
	
    solve_with_SCC(reduced, result);
    resolveReverseRules(result, reverseRules);
    return result;
}

std::string cyclebranch_solver::description() const{
    return "Cycle branch solver";
}
