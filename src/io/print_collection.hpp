#include <iostream>
#include <vector>
#include <utility>
#include <set>

using std::ostream;
using std::vector;
using std::pair;
using std::set;

template <typename T>
void _print(const T & a, ostream & os){
    os << a;
}

template <typename T>
void _print(const pair<T, T> & v, ostream & os){
    os << "(";
    _print(v.first, os);
    os << ", ";
    _print(v.second, os);
    os << ")";
}

template <typename T>
void _print(const set<T> & s, ostream & os){
    os << "{";
    size_t i = 0;
    for(T t : s){
        ++i;
        _print(t, os);
        if(i != s.size()){
            os << ", ";
        }
    }
    os << "}";
}

template <typename T>
void _print(const vector<T> & v, ostream & os){
    os << "[";
    for (size_t i = 0; i < v.size() - 1; ++i){
        _print(v[i], os);
        os << ", ";
    }
    if(v.size() != 0){
        _print(v[v.size()-1], os);
    }
    os << "]";
}

template <typename T>
void print(T t, ostream & os){
    _print(t, os);
    os << std::endl;
}


