#pragma once
#include <ostream>

template<typename Graph>
void output_solution(std::ostream& os, const typename Graph::vertex_vec_t & solution);