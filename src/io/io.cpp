#include <sstream>
#include <istream>
#include <string>
#include <map>
#include "io.hpp"
#include "../graph/directed_graph.hpp"


std::istream& get_uncommented_line(std::istream& is, std::string& line)
{
	do
		getline(is, line);
	while(line[0] == '%');
	return is;
}

/*!
 * format: https://pacechallenge.org/2022/tracks/#input-format
 */
template<>
directed_graph load_graph<directed_graph>(std::istream& is)
{
	std::string line;
	std::istringstream line_stream;
	get_uncommented_line(is, line);
	line_stream.str(line);
	size_t vertices, outcoming_edges, tmp;
	line_stream >> vertices >> outcoming_edges >> tmp;
	assert(tmp == 0); //graphs can be only unweighted
	directed_graph result(vertices);
	for(size_t u = 0; u < vertices; ++u)
	{
		get_uncommented_line(is, line);
		line_stream.str(line);
		line_stream.clear(); //the stream might have failed (reached eof before) could be done better, maybe return new stringstream object from get_uncommented_line method ?
		size_t v;
		while(line_stream >> v)
			result.add_edge(u, v - 1); // vertices are indexed from 1 but we index them from 0
	}
	return result;
}

/*!
 * format: https://pacechallenge.org/2022/tracks/#input-format
 */
template<>
void output_graph<directed_graph>(std::ostream& os, const directed_graph& graph)
{
    std::map<int,int> graph_v_to_out_v;
    std::map<int,int> out_v_to_graph_v;
    for(int u : graph.get_vertices()) {
        graph_v_to_out_v[u] = graph_v_to_out_v.size();
        out_v_to_graph_v[graph_v_to_out_v[u]] = u;
    }

    os << graph.get_vertices_count() << " " << graph.get_edges_count() << " 0\n";
    //Really assume we have vertices 0, ... ,N-1 otherwise nasty things happen...
    for(size_t u = 0; u < graph.get_vertices_count(); ++u, os << '\n')
    {
        int out_u = out_v_to_graph_v[u];
        for(auto vit = graph.successors(out_u).begin(), end = graph.successors(out_u).end(); vit != end; ++vit, os << ' ')
        {
            os << graph_v_to_out_v[*vit] + 1; //index + 1
        }
    }
    os.flush();
}

std::string graph_to_json(const directed_graph & g) {
    std::string s;
    s += "{\"vertices\":[";
    for(int v : g.get_vertices_vec()) {
        s+=std::to_string(v)+",";
    }
    if(s[s.size() -1 ] != '['){
        s.pop_back();
    }
    s += "],\"edges\":[";
    for(int v : g.get_vertices_vec()) {
        for(int u : g.successors(v)) {
            s+="["+std::to_string(v)+","+std::to_string(u)+"],";
        }
    }
    if(s[s.size() -1 ] != '['){
        s.pop_back();
    }
    s += "]}";
    return s;
}

template<>
void output_graph_as_json(std::ostream& os,const directed_graph& graph){
	os << graph_to_json(graph);
}