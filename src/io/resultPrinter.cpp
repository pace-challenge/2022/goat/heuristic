#include "resultPrinter.hpp"

#include "../graph/directed_graph.hpp"

template <>
void output_solution<directed_graph>(std::ostream &os, const typename directed_graph::vertex_vec_t &solution)
{
    for (const auto &v : solution)
        os << v + 1 << '\n';
    os << std::flush;
}