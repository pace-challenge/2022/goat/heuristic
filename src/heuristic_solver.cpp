#include <iostream>
#include <functional>
#include <random>
#include <chrono>
#include "measuring/util/timer.h"

#include "upperbounds/heuristic_ub.hpp"
#include "io/io.hpp"
#include "solver/solver.hpp"

int main(void)
{
    TREE_TIMER(ttload,"load","");
	directed_graph graph = load_graph<directed_graph>(std::cin);
    TREE_TIMER_STOP(ttload);

	auto dfvs = heuristic_ub().solve_dfvs(graph);

#ifdef __DEBUG__
	for(auto v : dfvs){
		graph.remove_vertex(v);
	}
	std::cout << dfvs.size() << std::endl;
	for(directed_graph::vertex_t v: dfvs)
	{
		std::cout << v << std::endl;
	}
	assert(is_DAG(graph));
#else
	output_solution<directed_graph>(std::cout, dfvs);
#endif
	return 0;
}

