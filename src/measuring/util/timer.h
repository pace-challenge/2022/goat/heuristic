#pragma once

#include <sstream>
#include <string>
#include <chrono>
#include <functional>
#include <iostream>
#include <cassert>
#include <type_traits>
#include <random>


/*!
 * @brief RAII timer that prints the time in [ms] by default
 */
struct TIMER
{
    std::function<void(double)> f;
    decltype(std::chrono::high_resolution_clock::now()) begin;
    bool stopped = false;
    double totalTime = 0;

    /**
     * Constructor
     * @param func call function to handle the measured time
     */
    TIMER(std::function<void(double)> func = [](double res) { std::cout << res << std::endl; })
        : f(func)
        { start(true);}

    ~TIMER()
    {
        if (!stopped)
            stop(true);
    }

    double stop(bool invokeFunction)
    {
        auto end = std::chrono::high_resolution_clock::now();
        assert(!stopped && "tried to stop a not running timer");
        stopped = true;
        totalTime += (std::chrono::duration_cast<std::chrono::seconds>(end - begin).count());
        if (invokeFunction) f(totalTime);
        return totalTime;
    }

    void start(bool resetTimer)
    {
        if (resetTimer) totalTime = 0;
        stopped = false;
        begin = std::chrono::high_resolution_clock::now();
    }

    double getCurTime() const { return totalTime; }
};

extern std::mt19937_64 gen64;

void args_to_str(std::ostringstream & oss);

template<class T, class... Args>
void args_to_str(std::ostringstream & oss, T value, Args... args){
    oss << value;
    args_to_str(oss, args...);
}

template<class T, class... Args>
std::string args_to_str(T value, Args... args){
    std::ostringstream buffer;
    args_to_str(buffer, value, args...);
    return buffer.str();
}

#define TREE_TIMER(tt,a,b) _TREE_TIMER tt(a,b);
#define TREE_TIMER_STOP(tt) tt.stop();

// #define TREE_TIMER(tt,a,b)
// #define TREE_TIMER_STOP(tt)


struct _TREE_TIMER
{
    decltype(std::chrono::high_resolution_clock::now()) begin;
    unsigned long long r;
    std::string id, data;
    bool stopped;
    bool measure_bias;
    int bias_dur_ns = 0;

    _TREE_TIMER(std::string id, std::string data, bool measure_bias=true)
        : r(gen64()),id(id), data(data), stopped(false), measure_bias(measure_bias)
        {
            if(measure_bias) {
                bias_dur_ns = do_measure_bias();
            }
            if(measure_bias) {
                std::cerr<<"_TREE_TIMER;START;r="<<r<<";id="<<id<<";"<<data<<std::endl;
            }
            begin = std::chrono::high_resolution_clock::now();
        }

    ~_TREE_TIMER() {
        if(!stopped) stop();
    }

    int do_measure_bias() {
        int total_bias_dur_ns = 0;
        int nn = 10;
        for(int i = 0; i < nn; ++i) {
             _TREE_TIMER ttx("","",false);
            ttx.stop();
            auto bias_dur_ns =(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - ttx.begin).count());
            total_bias_dur_ns += bias_dur_ns;
        }
        return total_bias_dur_ns / nn;
    }

    void stop() {
        auto end = std::chrono::high_resolution_clock::now();

        if(stopped) return;
        stopped = true;

        auto dur_ns =(std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count());
        dur_ns -= bias_dur_ns;
        if(dur_ns<0) dur_ns = 0;
        if(measure_bias) {
            std::cerr<<"_TREE_TIMER;END;r="<<r<<";id="<<id<<";dur_ns="<<dur_ns<<";dur_ms="<<dur_ns/1000/1000<<";"<<data<<std::endl;
        }
    }
};

