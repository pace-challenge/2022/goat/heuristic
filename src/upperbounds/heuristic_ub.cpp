#include "heuristic_ub.hpp"
#include "../graph/utils.hpp"
#include "../graph/bitset.hpp"
#include "../reduction_rules/combinations.hpp"
#include "../solver/cyclebranch.hpp"
#include "../lowerbounds/cyclepacking.hpp"
#include "../measuring/util/timer.h"

#include "../reduction_rules/sourcesink.hpp"
#include "../reduction_rules/remove_self_loops.hpp"
#include "../reduction_rules/edgecontraction.hpp"
#include "../reduction_rules/combinations.hpp"

#include <map>
#include <set>
#include <iostream>
#include <random>
#include <chrono>

using namespace std;

using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_set_t = directed_graph::vertex_set_t;

const long long int REDUCTION_TIME_LIMIT_MS = 2LL * 60 * 1000;
const long long int IMPROVEMENT_AND_REDUCTION_TIME_LIMIT_MS = 9LL * 60 * 1000;

vertex_vec_t vs_sorted_by_deg_desc(const directed_graph& graph) {
    vertex_vec_t vs = graph.get_vertices_vec();
    sort(vs.begin(), vs.end(), [&graph](int u, int v) {
        int udeg = graph.in_degree(u) + graph.out_degree(u);
        int vdeg = graph.in_degree(v) + graph.out_degree(v);
        return udeg > vdeg;
    });
    return vs;
}

int get_max_degree_v(const directed_graph& graph) {
    int mv = -1;
    int mdeg = -1;
    for(vertex_t v : graph.get_vertices()){
        int sum_degrees = graph.in_degree(v) + graph.out_degree(v);
        if(sum_degrees > mdeg) {
            mdeg = sum_degrees;
            mv = v;
        }
    }
    assert(mv>=0);
    return mv;
}

int get_min_degree_v(const directed_graph& graph) {
    int mv = -1;
    int mdeg = graph.get_vertices_count()+1;
    for(vertex_t v : graph.get_vertices()){
        int sum_degrees = graph.in_degree(v) + graph.out_degree(v);
        if(sum_degrees < mdeg) {
            mdeg = sum_degrees;
            mv = v;
        }
    }
    assert(mv>=0);
    return mv;
}


bool try_improve_solution_is_solution(const directed_graph&graph, const vertex_vec_t& sol) {
    auto g = graph;
    for(int v : sol) {
        if(v==-1)continue;
        if(!g.contains_vertex(v))continue;
        g.remove_vertex(v);
    }
    return is_DAG(g);
}

void unremove_vertex(directed_graph & g_out, int v, const directed_graph& g) {
    assert(!g_out.contains_vertex(v));
    g_out.add_vertex(v);
    for(int u : g.predecessors(v)) {
        if(g_out.contains_vertex(u)) g_out.add_edge(u, v);
    }
    for(int u : g.successors(v)) {
        if(g_out.contains_vertex(u)) g_out.add_edge(v, u);
    }
}


auto transform_directed_graph_to_simple_graph(const directed_graph & g) {
    // translation
    int n = g.get_vertices_count();
    vector<vector<int>> adj(n);
    map<int,int> dv_to_v;
    vector<int> v_to_dv;
    for(int dv : g.get_vertices()) {
        dv_to_v[dv] = dv_to_v.size();
        v_to_dv.push_back(dv);
    }
    set<pair<int,int>> edges;
    for(int dv : g.get_vertices()) {
        int v = dv_to_v[dv];
        for(int du : g.successors(dv)) {
            int u = dv_to_v[du];
            adj[v].push_back(u);
            edges.insert({v,u});
        }
    }

    return make_tuple(n, adj, edges, dv_to_v, v_to_dv);
}

bool there_is_a_cycle_including_v_fast(int n, const vector<vector<int>> & adj, const set<pair<int,int>> & edges, const vector<bool> & usable, int v, _DynamicBitset & seen) {
    seen.clear(0);
    queue<int> q;
    q.push(v);
    seen.set_bit(v);
    while(q.size()) {
        int u = q.front();
        assert(usable[u]);
        q.pop();
        for(int nu : adj[u]) {
            if(!usable[nu]) continue;

            if(!seen.has_bit(nu)) {
                q.push(nu);
                seen.set_bit(nu);
            }
            else if(nu == v){
                return true;
            }
        }
    }
    return false;
}

vector<int> get_a_shortest_cycle_including_v_fast(int n, const vector<vector<int>> & adj, const set<pair<int,int>> & edges, const vector<bool> & usable, int v, vector<int> & parent) {
    vector<int> visited;
    visited.push_back(v);
    queue<int> q;
    q.push(v);
    parent[v] = v;
    while(q.size()) {
        int u = q.front();
        assert(usable[u]);
        q.pop();
        for(int nu : adj[u]) {
            if(!usable[nu]) {
                continue;
            }

            if(parent[nu] == -1) {
                q.push(nu);
                parent[nu] = u;
                visited.push_back(nu);
            }
            else if(nu == v){
                vector<int> cycle;
                while(1) {
                    cycle.push_back(u);
                    u = parent[u];
                    if(u == v) break;
                }
                for(int u : visited){parent[u]=-1;}
                return cycle;
            }
        }
    }
    for(int u : visited){parent[u]=-1;}
    return {};
}


directed_graph::vertex_vec_t try_improve_solution_fast(const directed_graph& g, const directed_graph::vertex_vec_t & dv_solution) {
    TREE_TIMER(tt,"try_improve_solution_fast", args_to_str("n=", g.get_vertices_count(), " m=", g.get_edges_count()));

    auto [n, adj, edges, dv_to_v, v_to_dv] = transform_directed_graph_to_simple_graph(g);
    vector<int> solution;
    for(int dv : dv_solution){
        solution.push_back(dv_to_v[dv]);
    }

    vector<bool> usable(n, true);
    for(int v : solution) {
        usable[v] = false;
    }

    _DynamicBitset there_is_a_cycle_including_v_fast_seen(n);

    vector<int> new_solution;
    for(int v : solution) {
        usable[v] = true;

        if(there_is_a_cycle_including_v_fast(n, adj, edges, usable, v, there_is_a_cycle_including_v_fast_seen)) {
            new_solution.push_back(v);
            usable[v] = false;
        }
    }

    vector<int> new_dv_solution;
    for(int v : new_solution) {
        new_dv_solution.push_back(v_to_dv[v]);
    }
    cerr<<"try_improve_solution_fast "<<dv_solution.size()<<" => "<<new_dv_solution.size()<<endl;
    return new_dv_solution;
}

// directed_graph::vertex_vec_t try_improve_solution(const directed_graph& g, const directed_graph::vertex_vec_t & solution, int keep_v = -1) {
//     TREE_TIMER(tt,"try_improve_solution", args_to_str("n=", g.get_vertices_count(), " m=", g.get_edges_count()));

//     directed_graph g1 = g;
//     for(int v : solution) {
//         g1.remove_vertex(v);
//     }

//     vertex_vec_t new_solution;
//     for(int v : solution) {
//         if(v==keep_v) {
//             new_solution.push_back(v);
//             continue;
//         }

//         unremove_vertex(g1, v, g);

//         int v_2way_cnt = 0;
//         for(int u : g1.successors(v)) {
//             if(g1.contains_edge(u, v)) {
//                 v_2way_cnt++;
//                 if(v_2way_cnt>=1) break;
//             }
//         }
//         // surely not dag
//         if(v_2way_cnt >= 1) {
//             new_solution.push_back(v);
//             g1.remove_vertex(v);
//             continue;
//         }

//         // we unremoved v, so if it is not DAG, there will be a cycle containing v
//         auto new_v_cycle = find_shortest_cycle_including_v(g1, v);
//         // if(!is_DAG(g1)) {
//         if(new_v_cycle.size()!=0) {
//             new_solution.push_back(v);
//             g1.remove_vertex(v);
//         }
//     }
//     assert(try_improve_solution_is_solution(g, new_solution));
//     return new_solution;
// }

directed_graph::vertex_vec_t try_exchange_solution_fast(const directed_graph& g, const directed_graph::vertex_vec_t & dv_solution, decltype(std::chrono::steady_clock::now()) solve_dfvs_start) {
    TREE_TIMER(tt,"try_exchange_solution_fast", args_to_str("n=", g.get_vertices_count(), " m=", g.get_edges_count()));

    auto [n, adj, edges, dv_to_v, v_to_dv] = transform_directed_graph_to_simple_graph(g);
    vector<int> solution;
    for(int dv : dv_solution){
        solution.push_back(dv_to_v[dv]);
    }

    vector<vector<int>> pred(n);
    for(auto e : edges) {
        pred[e.second].push_back(e.first);
    }

    vector<bool> usable(n, true);
    for(int v : solution) {
        usable[v] = false;
    }
    vector<vector<int>> usable_adj(n);
    for(int v = 0; v < n; ++v) {
        for(int u : adj[v]) {
            if(usable[u]) {
                usable_adj[v].push_back(u);
            }
        }
    }



    map<int,vector<int>> exchangable;

    vector<int> parent(n, -1);

    map<int, vector<int>> exchangees;

    int xxx = 0;
    for(int v : solution) {
        xxx++;
        if(xxx%100==0) {
            cerr<<"xxx="<<xxx<<"/"<<solution.size()<<endl;
        }

        auto solve_dfvs_so_far_dur_ms = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - solve_dfvs_start).count();
        if(solve_dfvs_so_far_dur_ms>=IMPROVEMENT_AND_REDUCTION_TIME_LIMIT_MS) {
            break;
        }

        usable[v] = true;
        for(int u : pred[v]) {
            usable_adj[u].push_back(v);
        }

        auto v_cycle = get_a_shortest_cycle_including_v_fast(n, usable_adj, edges, usable, v, parent);
        for(int u : v_cycle) {
            if(u==v) continue;
            assert(usable[u]);
            usable[u] = false;
            auto new_v_cycle = get_a_shortest_cycle_including_v_fast(n, usable_adj, edges, usable, v, parent);
            if(new_v_cycle.size() == 0) {
                exchangable[u].push_back(v);
            }
            usable[u] = true;
        }
        usable[v] = false;
        for(int u : pred[v]) {
            usable_adj[u].pop_back();
        }
    }
    // try_improve_solution_fast 15193 => 15193
    // exchangable=12889
    cerr<<"exchangable="<<exchangable.size()<<endl;
    // cerr<<"new_v_cycle_zero_cnt="<<new_v_cycle_zero_cnt<<endl;
    // cerr<<"new_v_cycle_not_zero_cnt="<<new_v_cycle_not_zero_cnt<<endl;
    // for(auto e : new_v_cycle_not_zero_size) {
    //     cerr<<"new_v_cycle_not_zero_size "<<e.first<<" => "<<e.second<<endl;
    // }
    // for(auto e : v_cycle_size) {
    //     cerr<<"v_cycle_size "<<e.first<<" => "<<e.second<<endl;
    // }



    auto i_dv_solution = dv_solution;
    for(auto [v, e_vs] : exchangable) {
        if(e_vs.size() <= 1) continue;
        // cerr<<"e_vs="<<v<<" "<<e_vs.size()<<endl;
        i_dv_solution.push_back(v_to_dv[v]);
    }

    auto new_i_dv_solution = try_improve_solution_fast(g, i_dv_solution);

    return new_i_dv_solution;
}

// directed_graph::vertex_vec_t try_exchange_solution(const directed_graph& g, const directed_graph::vertex_vec_t & solution) {
//     TREE_TIMER(tttes,"try_exchange_solution", args_to_str("n=", g.get_vertices_count(), " m=", g.get_edges_count()));

//     TREE_TIMER(tttes1,"try_exchange_solution 1", args_to_str("n=", g.get_vertices_count(), " m=", g.get_edges_count()));
//     directed_graph g1 = g;
//     for(int v : solution) {
//         g1.remove_vertex(v);
//     }
//     assert(is_DAG(g1));
//     TREE_TIMER_STOP(tttes1);

//     vector<int> exchange_vs;
//     for(int v : g1.get_vertices()) exchange_vs.push_back(v);

//     cerr<<"solution="<<solution.size()<<endl;
//     cerr<<"exchange_vs="<<exchange_vs.size()<<endl;

//     TREE_TIMER(tttes2,"try_exchange_solution 2", args_to_str("n=", g.get_vertices_count(), " m=", g.get_edges_count()));

//     map<int,vector<int>> exchangable;

//     for(int v : solution) {
//         // before this, g1 is DAG
//         // so when we unremove v, we may introduce some cycles, but these cycles must contain v
//         unremove_vertex(g1, v, g);

//         // we first check if we created at least 2 two way edges => that means we created two cycles disjoint cycles (except for v)
//         int v_2way_cnt = 0;
//         for(int u : g1.successors(v)) {
//             if(g1.contains_edge(u, v)) {
//                 v_2way_cnt++;
//                 if(v_2way_cnt>=2) break;
//             }
//         }
//         if(v_2way_cnt >= 2) {
//             g1.remove_vertex(v);
//             continue;
//         }

//         auto v_cycle = find_shortest_cycle_including_v(g1, v);
//         for(int u : v_cycle) {
//             if(u==v) continue;
//             g1.remove_vertex(u);
//             // TODO: zkontrolovat a udrzovat pocty obousmernejch hran z v
//             // TODO: najit kruznici z v pres shortest cycle
//             auto new_v_cycle = find_shortest_cycle_including_v(g1, v);
//             if(new_v_cycle.size() == 0) {
//                 // assert(is_DAG(g1));
//                 exchangable[u].push_back(v);

//             }
//             // if(is_DAG(g1)) {
//             //     exchangable[u].push_back(v);
//             // }
//             unremove_vertex(g1, u, g);
//         }
//         g1.remove_vertex(v);
//     }

//     cerr<<"exchangable="<<exchangable.size()<<endl;

//     auto i_solution = solution;
//     for(auto [v, e_vs] : exchangable) {
//         if(e_vs.size() <= 1) continue;
//         // cerr<<"e_vs="<<v<<" "<<e_vs.size()<<endl;
//         i_solution.push_back(v);
//     }
//     TREE_TIMER_STOP(tttes2);


//     auto new_i_solution = try_improve_solution_fast(g, i_solution);
//     return new_i_solution;
// }

// directed_graph::vertex_vec_t try_exactly_improve_solution(const directed_graph& g, const directed_graph::vertex_vec_t & solution) {
//     cyclebranch_solver cbs;
//     directed_graph g1 = g;
//     for(int v : solution) {
//         g1.remove_vertex(v);
//     }

//     return solution;

//     vertex_vec_t new_solution;

//     int k = 10;
//     int i = -1;
//     while(1) {
//         i++;
//         if(i+k>=solution.size())break;
//         vector<int> solution_part;
//         for(int j = 0; j<k;++j) {
//             solution_part.push_back(solution[i+j]);
//         }
//         if(solution_part.size()==0)break;

//         for(int v : solution_part) {
//             unremove_vertex(g1, v, g);
//         }
//         directed_graph g2 = g1;
//         auto exact_solution_part = cbs.solve_dfvs(g2);
//         std::cerr<<"i="<<i<<" exact="<<exact_solution_part.size()<<" part="<<solution_part.size()<<std::endl;
//         if(exact_solution_part.size()<solution_part.size()) {
//             for(int v : exact_solution_part){
//                 new_solution.push_back(v);
//                 g1.remove_vertex(v);
//             }
//         }
//         else {
//             for(int v : solution_part){
//                 new_solution.push_back(v);
//                 g1.remove_vertex(v);
//             }
//         }
//     }

//     assert(try_improve_solution_is_solution(g, new_solution));
//     return new_solution;
// }


directed_graph::vertex_vec_t heuristic_ub::solve_dfvs(const directed_graph& g){
    auto solve_dfvs_start = std::chrono::steady_clock::now();

    map<int,int> out_reduction_counter;

    directed_graph graph2 = g;
    cerr<<"graph2.n="<<graph2.get_vertices_count()<<" graph2.m="<<graph2.get_edges_count()<<endl;

    vertex_vec_t removed2;
    reverse_rules_stack<directed_graph> reverseRules;

    TREE_TIMER(ttreduce_heuristic,"reduce_heuristic", args_to_str("n=", graph2.get_vertices_count(), " m=", graph2.get_edges_count()));
    reduce_heuristic(graph2, removed2, reverseRules, out_reduction_counter);
    TREE_TIMER_STOP(ttreduce_heuristic);
    cerr<<"after graph2.n="<<graph2.get_vertices_count()<<" graph2.m="<<graph2.get_edges_count()<<" removed2="<<removed2.size()<< endl;


    directed_graph graph = graph2;
    vertex_vec_t removed;

    // TODO: removed po prvnich redukcich jsou urcite spravne
    // TODO: lze jeste pamatovat, ktere vrcholy

    // TODO: lze predelat vybirani max deg vrcholu tak, aby jich vybral rovnou vic najednou - zrusim ten for s progress_speed


    int progress_speed = 1;
    int progress_iterations = 0;

    int progress_time_remaining_ms = REDUCTION_TIME_LIMIT_MS - std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - solve_dfvs_start).count();

    cerr<<"progress_time_remaining_ms="<<progress_time_remaining_ms<<endl;

    auto progress_start = std::chrono::steady_clock::now();
    int progress_iteration_list_size = 5;
    vector<int> progress_iteration_dur_ms_list(progress_iteration_list_size, 0);
    vector<int> progress_iteration_vs_list(progress_iteration_list_size, 0);

    while(!is_DAG(graph)) {
        TREE_TIMER(ttreduce_heuristic_iter,"reduce_heuristic_iter", args_to_str("n=", graph.get_vertices_count(), " m=", graph.get_edges_count()));
        auto progress_iteration_start = std::chrono::steady_clock::now();
        int progress_iteration_start_vs = graph.get_vertices_count();


        TREE_TIMER(ttps,"ttps", args_to_str("n=", graph.get_vertices_count(), " m=", graph.get_edges_count()));


        map<int, set<int>> deg_v;
        map<int, int> v_to_deg;
        for(int v : graph.get_vertices()) {
            deg_v[graph.in_degree(v) + graph.out_degree(v)].insert(v);
            v_to_deg[v] = graph.in_degree(v) + graph.out_degree(v);
        }
        for(int i = 0; i < progress_speed; ++i) {
            int v = -1;
            while(1) {
                if(deg_v.rbegin()->second.size() == 0) {
                    deg_v.erase(deg_v.rbegin()->first);
                    continue;
                }
                v = *(deg_v.rbegin()->second.begin());
                deg_v.rbegin()->second.erase(v);
                break;
            }
            assert(v!=-1);
            v_to_deg.erase(v);
            for(int u : graph.successors(v)) {
                deg_v[v_to_deg[u]].erase(u);
                v_to_deg[u]--;
                deg_v[v_to_deg[u]].insert(u);
            }
            for(int u : graph.predecessors(v)) {
                deg_v[v_to_deg[u]].erase(u);
                v_to_deg[u]--;
                deg_v[v_to_deg[u]].insert(u);
            }

            removed.push_back(v);
            graph.remove_vertex(v);
            if(graph.get_vertices_count() == 0) break;
        }
        TREE_TIMER_STOP(ttps);

        reduce_heuristic(graph, removed, reverseRules, out_reduction_counter);

        bool reduce_heuristic_removed_something = progress_iteration_start_vs != graph.get_vertices_count();

        progress_iterations++;

        if(graph.get_vertices_count() == 0) break;

        auto progress_iteration_end = std::chrono::steady_clock::now();
        auto progress_iteration_dur_ms = std::chrono::duration_cast<std::chrono::milliseconds>(progress_iteration_end - progress_iteration_start).count();
        progress_iteration_dur_ms_list[progress_iterations % progress_iteration_list_size] = progress_iteration_dur_ms;

        int progress_iteration_removed_vs = progress_iteration_start_vs - graph.get_vertices_count();
        progress_iteration_vs_list[progress_iterations % progress_iteration_list_size] = progress_iteration_removed_vs;

        if(progress_iteration_dur_ms < 20) {
            continue;
        }

        double progress_iteration_dur_ms_avg = progress_iteration_dur_ms;
        double progress_iteration_removed_vs_avg = progress_iteration_removed_vs;
        if(progress_iterations > progress_iteration_list_size) {
            progress_iteration_dur_ms_avg = 0;
            for(int dd : progress_iteration_dur_ms_list) progress_iteration_dur_ms_avg+=dd;
            progress_iteration_dur_ms_avg /= progress_iteration_list_size;

            progress_iteration_removed_vs_avg = 0;
            for(int dd : progress_iteration_vs_list) progress_iteration_removed_vs_avg+=dd;
            progress_iteration_removed_vs_avg /= progress_iteration_list_size;

        }

        auto progress_so_far_dur_ms = std::chrono::duration_cast<std::chrono::milliseconds>(progress_iteration_end - progress_start).count();

        if(progress_iteration_removed_vs == 0) {
            continue;
        }

        double time_ETA_ms = (double)graph.get_vertices_count() / progress_iteration_removed_vs_avg * progress_iteration_dur_ms_avg;

        auto time_remaining_ms = (progress_time_remaining_ms - progress_so_far_dur_ms);

        if(time_remaining_ms <= 0) {
            progress_speed = graph.get_vertices_count();
            continue;
        }

        double speedup = time_ETA_ms / time_remaining_ms;

        double clamped_speedup = min(max(speedup, 1/1.618), 1.618);

        int new_progress_speed = progress_speed * clamped_speedup;
        // we can afford to slow down only if the reduction did something and we are actually ahead
        if(reduce_heuristic_removed_something && clamped_speedup < 1) {
            new_progress_speed--;
        }
        else {
            new_progress_speed ++;
        }

        if(new_progress_speed <= 0) new_progress_speed = 1;

        std::cerr<<"progress_speed="<<progress_speed<<std::endl;
        std::cerr<<"progress_iteration_dur_ms="<<progress_iteration_dur_ms<<std::endl;
        std::cerr<<"progress_iteration_dur_ms_avg="<<progress_iteration_dur_ms_avg<<std::endl;
        std::cerr<<"progress_so_far_dur_ms="<<progress_so_far_dur_ms<<std::endl;
        std::cerr<<"progress_iteration_removed_vs="<<progress_iteration_removed_vs<<std::endl;
        std::cerr<<"progress_iteration_removed_vs_avg="<<progress_iteration_removed_vs_avg<<std::endl;
        std::cerr<<"time_ETA_ms="<<time_ETA_ms<<std::endl;
        std::cerr<<"time_remaining_ms="<<time_remaining_ms<<std::endl;
        std::cerr<<"speedup="<<speedup<<std::endl;
        std::cerr<<"clamped_speedup="<<clamped_speedup<<std::endl;
        std::cerr<<"new_progress_speed="<<new_progress_speed<<std::endl;


        progress_speed = new_progress_speed;
    }

    // while(!is_DAG(graph)) {
    //     TREE_TIMER(ttreduce_heuristic_iter,"reduce_heuristic_iter", args_to_str("n=", graph.get_vertices_count(), " m=", graph.get_edges_count()));
    //     for(int i = 0; i < progress_speed; ++i) {
    //         int v = get_min_degree_v(graph);
    //         notInSolution(graph, v);
    //         if(graph.get_vertices_count() == 0) break;
    //     }

    //     if(!reduce_heuristic(graph, removed, reverseRules)) {
    //         // progress_speed++;
    //     }
    //     else {
    //         // progress_speed--;
    //     }
    // }





    resolveReverseRules(removed, reverseRules);

    auto solution = removed;

    assert(try_improve_solution_is_solution(graph2, solution));

    while(1) {
        auto solve_dfvs_so_far_dur_ms = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - solve_dfvs_start).count();
        std::cerr<<"solve_dfvs_so_far_dur_ms="<<solve_dfvs_so_far_dur_ms<<std::endl;
        if(solve_dfvs_so_far_dur_ms >= IMPROVEMENT_AND_REDUCTION_TIME_LIMIT_MS) break;

        {
        cerr<<"try_improve_solution_fast"<<endl;
        auto new_solution = try_improve_solution_fast(graph2, solution);
        if(new_solution.size() < solution.size()) {
            cerr<<"try_improve_solution_fast "<<solution.size()<<" => "<<new_solution.size()<<endl;
            solution = new_solution;
            continue;
        }
        }

        // {
        // cerr<<"try_improve_solution"<<endl;
        // auto new_solution = try_improve_solution(graph2, solution);
        // if(new_solution.size() < solution.size()) {
        //     cerr<<"try_improve_solution "<<solution.size()<<" => "<<new_solution.size()<<endl;
        //     solution = new_solution;
        //     continue;
        // }
        // }
        // {
        // cerr<<"try_exactly_improve_solution"<<endl;
        // auto new_solution = try_exactly_improve_solution(graph2, solution);
        // if(new_solution.size() < solution.size()) {
        //     cerr<<"try_exactly_improve_solution "<<solution.size()<<" => "<<new_solution.size()<<endl;
        //     solution = new_solution;
        //     continue;
        // }
        // }
        {
        cerr<<"try_exchange_solution_fast"<<endl;
        auto new_solution = try_exchange_solution_fast(graph2, solution, solve_dfvs_start);
        if(new_solution.size() < solution.size()) {
            cerr<<"try_exchange_solution_fast "<<solution.size()<<" => "<<new_solution.size()<<endl;
            solution = new_solution;
            continue;
        }
        }
        // {
        // cerr<<"try_exchange_solution"<<endl;
        // auto new_solution = try_exchange_solution(graph2, solution);
        // if(new_solution.size() < solution.size()) {
        //     cerr<<"try_exchange_solution "<<solution.size()<<" => "<<new_solution.size()<<endl;
        //     solution = new_solution;
        //     continue;
        // }
        // }



        break;
    }

    cerr<<"solution="<<solution.size()<<endl;

    for(int v : removed2) {
        solution.push_back(v);
    }

    cerr<<"solution with remove="<<solution.size()<<endl;

    return solution;
}

string heuristic_ub::description() const{
    return "heuristic_ub";
}