#include "maxdegree.hpp"
#include "../graph/utils.hpp"

using namespace std;

using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_set_t = directed_graph::vertex_set_t;

int get_nontrivial_components(const vector<vertex_vec_t> & components){
    int count = 0;
    for(const vertex_vec_t & component : components){
        if(component.size() > 1){
            ++count;
        }
    }
    return count;
}

directed_graph::vertex_vec_t max_degree_ub::solve_dfvs(const directed_graph& g){
    directed_graph graph = g;
    vertex_vec_t removed;
    vector<vertex_set_t> degree_buckets = vector<vertex_set_t>(2*graph.get_vertices_count());
    for(vertex_t v : graph.get_vertices()){
        size_t sum_degrees = graph.in_degree(v) + graph.out_degree(v);
        degree_buckets[sum_degrees].insert(v);
    }
    size_t current_bucket = degree_buckets.size() - 1;
    vector<vertex_vec_t> sccs = strongly_connected_components(graph);
    while (sccs.size() < graph.get_vertices_count()){
        int nontrivial_components = get_nontrivial_components(sccs);
        for(int i = 0; i < nontrivial_components; ++i){
            while(degree_buckets[current_bucket].empty()){
                --current_bucket;
            }
            vertex_t v = *degree_buckets[current_bucket].begin();
            degree_buckets[current_bucket].erase(v);
            // recalculate neighbours - move them to lower buckets
            for(vertex_t succ : graph.successors(v)){
                if(succ == v){
                    continue;
                }
                size_t sum_degrees = graph.in_degree(succ) + graph.out_degree(succ);
                degree_buckets[sum_degrees].erase(succ);
                degree_buckets[sum_degrees-1 - graph.contains_edge(succ, v)].insert(succ); // if v is both predecessor and successor, it is moved by two buckets 
            }
            for(vertex_t pred : graph.predecessors(v)){
                if(pred == v || graph.contains_edge(v, pred)){ // if v is also successor it was handled in previous case
                    continue;
                }
                size_t sum_degrees = graph.in_degree(pred) + graph.out_degree(pred);
                degree_buckets[sum_degrees].erase(pred);
                degree_buckets[sum_degrees-1].insert(pred);
            }
            graph.remove_vertex(v);
            removed.push_back(v);
        }
        sccs = strongly_connected_components(graph); // calculate only when necessary
    }
    return removed;
}

string max_degree_ub::description() const{
    return "max degree upperbound";
}