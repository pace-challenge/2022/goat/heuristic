#pragma once

#include "../solver/abstract_solver.hpp"
#include "../graph/directed_graph.hpp"
#include "../reduction_rules/combinations.hpp"
#include "../graph/utils.hpp"

using namespace std;

using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;

/*!
 * Deletes vertices greedyly in order by scoring function. 
 * F is the type of the functor
 */
template <typename F>
class generic_ub : abstract_solver
{
 public:
	
	/*!
	* During solving: If all reduction rules are exhausted, every vertex is scored by the @scoring_function and vertex with highest score is deleted.
	*/
	generic_ub(F scoring_function): scoring_function(scoring_function){}
	virtual typename directed_graph::vertex_vec_t solve_dfvs(const directed_graph& g) override{
		directed_graph graph = g;
		vertex_vec_t removed;
		while (!is_DAG(graph)){
			reduce_all(graph, removed);
			if(is_DAG(graph)){
				return removed;
			}
			vertex_t to_del = *graph.get_vertices().begin();
			auto max_val =  scoring_function(graph, to_del);
			for(vertex_t v : graph.get_vertices()){
				auto score = scoring_function(graph, v);
				if(score > max_val){
					max_val = score;
					to_del = v;
				}
			}
			assert(to_del != -1);
			graph.remove_vertex(to_del);
			removed.push_back(to_del);
			// only do then there are at least two components to prevent copying of the graph
			if(strongly_connected_components(graph).size() > 1){
				vertex_vec_t removed_rec = calculate_on_components(graph, [this](const directed_graph & g){return this->solve_dfvs(g);});
				removed.insert(removed.end(), removed_rec.begin(), removed_rec.end());
				return removed;
			}
		}
		return removed;
	}

	virtual std::string description() const override{
		return "generic upperbound";
	}
private:
	F scoring_function; // takes graph and vertex
};






