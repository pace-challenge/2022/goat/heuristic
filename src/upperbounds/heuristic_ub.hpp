#pragma once

#include "../solver/abstract_solver.hpp"
#include "../graph/directed_graph.hpp"

#include <functional>

/*!
 * Deletes vertices with the highest degree until the graph is DAG
 */
class heuristic_ub : abstract_solver
{
 public:
    virtual typename directed_graph::vertex_vec_t solve_dfvs(const directed_graph& g) override;
    virtual std::string description() const override;
};






