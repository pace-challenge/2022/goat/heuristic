#include "cyclepacking.hpp"
#include "../graph/utils.hpp"
#include "../reduction_rules/combinations.hpp"
#include "lpvc.hpp"

#include <algorithm>
#include <cassert>


using namespace std;


using vertex_t = directed_graph::vertex_t;
using vertex_vec_t = directed_graph::vertex_vec_t;
using vertex_set_t = directed_graph::vertex_set_t;


// better cycle is the one that has smalle sum of degrees (second parameter)
// if the sum degree is small then size of the cycle is examined
bool better_cycle_cmp(const pair<vertex_vec_t, int> & a, const pair<vertex_vec_t, int> & b){
    if(a.second != b.second){
        return a.second < b.second;
    }
    return a.first.size() < b.first.size();
}

vector<vertex_vec_t> get_ordered_cycles(const directed_graph & graph){
    vector<vertex_vec_t> cycles = find_shortest_cycle_foreach_v(graph);
    vector<pair<vertex_vec_t, int>> cycles_with_deg;
    for(const vertex_vec_t & cycle : cycles){
        cycles_with_deg.push_back({cycle, sum_degrees(cycle, graph)});
    }
    sort(cycles_with_deg.begin(), cycles_with_deg.end(), better_cycle_cmp);
    assert(cycles.size() == cycles_with_deg.size());
    // putting the cycles back into correct structure
    for(size_t i = 0; i < cycles.size(); ++i){
        std::swap(cycles[i], cycles_with_deg[i].first);
    }
    return cycles;
}

//maybe the templates are unnecessary

// be cautious, the graph will change - 
template <typename F, typename S>
int packing_lb_reduce_immediatelly(directed_graph & graph, F get_structure, S score){
    int lb = 0;

    auto structure = get_structure(graph);
    while (!structure.empty())
    {
        for (directed_graph::vertex_t v : structure)
        {
            graph.remove_vertex(v);
        }
        lb += score(structure);
        reverse_rules_stack<directed_graph> rrs;
        vertex_vec_t removed;
        int k_diff = reduce_all(graph, removed, rrs);
        lb += k_diff;
        structure = get_structure(graph);
    }
    return lb;
}


// be cautious, the graph will change 
template <typename F, typename S>
int packing_lb_reduce_later(directed_graph & graph, F get_structure, S score){
    int lb = 0;

    auto structure = get_structure(graph);
    while (!structure.empty()){
        while (!structure.empty())
        {
            for (directed_graph::vertex_t v : structure)
            {
                graph.remove_vertex(v);
            }
            lb += score(structure);
            structure = get_structure(graph);
        }
        reverse_rules_stack<directed_graph> rrs;
        vertex_vec_t removed;
        int k_diff = reduce_all(graph, removed, rrs);
        lb += k_diff;
        structure = get_structure(graph);
    }
    return lb;
}


int pack_cycles_lb(directed_graph graph)
{
    int lb = 0;
    vector<vertex_vec_t> cycles = get_ordered_cycles(graph);
    vertex_vec_t removed;
    while (!cycles.empty())
    {
        for (const vertex_vec_t& cycle : cycles)
        {
            bool not_taken = true;
            for (directed_graph::vertex_t v : cycle)
            {
                not_taken = not_taken && graph.contains_vertex(v);
            }
            if (not_taken)
            {
                for (directed_graph::vertex_t v : cycle)
                {
                    graph.remove_vertex(v);
                }
                ++lb;
            }
        }
        reverse_rules_stack<directed_graph> rrs;
        int k_diff = reduce_all(graph, removed, rrs);
        lb += k_diff;
        cycles = get_ordered_cycles(graph);
    }
    return lb;
}


int lpvc_plus_pack_cycles_lb(directed_graph graph){
    int lb = lpvc_lb(graph);
    remove_2cycle_vertices_from_graph(graph);
    vertex_vec_t removed;
    reverse_rules_stack<directed_graph> rrs;
    int k_diff = reduce_all(graph, removed, rrs);
    lb += k_diff;
    lb += pack_cycles_lb(graph);
    return lb;
}

int pack_cliques_lb_slow(directed_graph graph){
    int lb = packing_lb_reduce_immediatelly(graph, [](const directed_graph &g){
        vertex_set_t clique = heuristic_maximal_clique(g);
        if(clique.size() <= 2){
            return vertex_set_t();
        }
        return clique;
    },
    [](const vertex_set_t & clique){return clique.size() -1;});
    return lb + lpvc_plus_pack_cycles_lb(graph);
}


int pack_cliques_lb(directed_graph graph){
    int lb = packing_lb_reduce_later(graph, [](const directed_graph &g){
        vertex_set_t clique = heuristic_maximal_clique(g);
        if(clique.size() <= 2){
            return vertex_set_t();
        }
        return clique;
    },
    [](const vertex_set_t & clique){return clique.size() -1;});
    return lb + lpvc_plus_pack_cycles_lb(graph);
}